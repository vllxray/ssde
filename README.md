# README #

The OpenSSDE calculation is a Python project, written and developed for Python 3.5.
The program is licensed under GPL-3.0.

This program is written to be possible to run alone but can advantageously be combined with [OpenREM](http://openrem.org/).

The program reads DICOM files and RDSR files imports the image data and predefined dicom tags.
Both axial and localizer images is imported.
The program is tested on dicom files from Philips Brilliance Big Bore, GE LigthSpeed VCT and Siemens Somatom Flash.
The program is tested on RDSR files from Siemens Somatom Flash and GE LigthSpeed VCT
(although RDSR-specific tags have not yet been predefined).

### Prerequisites ###

Python:

* Python 3.5
* numpy
* PyQt4
* Pydicom
* Scipy
* Matplotlib
* Skimage

## Table of contents

*    [Program structure](#markdown-header-program-structure)
*    [Program structure descriptions](#markdown-header-program-structure-descriptions)
    *    [Source comment structure](#markdown-header-source-comment-structure)
    *    [Import](#markdown-header-import)
    *    [Segmentation](#markdown-header-segmentation)
    *    [Patient size calculation](#markdown-header-patient-size-calculation)
    *    [Integration with OpenREM](#markdown-header-integration-with-openrem)
*    [Program development](#markdown-header-program-development)

## Program structure ##

* SSDEmain.py
![flow2.png](https://bitbucket.org/repo/kXzLqE/images/4073278277-flow2.png)


## Program structure descriptions ##

### Source comment structure ###

The source code is commented in accordance to PEP-257 with the following structure.

1. Short description of the function
2. Longer description of the function
3. Usage: Describing the purpose and output of the function
4. Uses: Describing dependencies as <Filename>-><Class>.<Function>()
5. Used by: Describing served functions as <Filename>-><Class>.<Function>()
6. Input parameters, e.g. :param <parameter>:
7. Output in accordance, e.g. :return:

#### Example ####

```
#!python
""" checks if dragged items contains url, then it is dropable

Verifies that an item dragged over the main window contains a url. This is done to make sure that the item is 
"dropable", i.e., that it is an item that can be searched for files to import.

If it contains a url, the function returns a signal to approve making the item "dropable". Otherwise the return
signal tells the caller to ignore this item.

Usage: Gives a visual signal to the user, showing if the item is dropable or not.
 
Uses: dcmImport.py-><ClassName>.<FunctionName>()

:param event:
:return:
"""
``` 

### Import ###

**Import of files.**

1. The program supports two ways of importing files:

    * A file, or directory, is dragged from the computer's file browser and dropped inside the program's window. The item is dropable if the dropped item contains a URL. The URL(s) is extracted and stored as a list. A signal, containing the list of file paths, is emitted invoking the function *somethingDropped*.

    * A file is chosen by a File Dialog invoked by a button click.
    The directory path is extracted from the file path and sent to the function *findAllFilesQt*.
    A generator object with all the files in the chosen files directory is returned and stored as a list. A signal, containing the list of file paths, is emitted invoking the function *somethingDropped*.

2.  The function *somethingDropped* receives a list of paths. If the first item of the list is a path to a directory then the list is sent to *findAllFilesQt* from which all file paths are returned and stored in a list. If the first item is a path to a single file then the list is simply set to the dropped list of files.

3.  Each file in the list is read by the function *validImage* which returns two parameters, success and info.

4.  If the file was read with success, i.e. it was an image that the program can process, then the image is added by sending info to the *datamanager*'s function *addImage*.

**Structure of the model **

The structure of the model follows the MVC design pattern.

```
#!python
Root
Series
    InfoContainer
        InfoItem
        .
        .
        .
    Localization (not utilized)
    Analysis (not utilized)
    ImageContainer
        Image
        .
        .
        .
```

Exempel på hur pythonkod kan skrivas in i denna wiki
```
#!python
import numpy as np
```

### Segmentation ###
Segmentation of axial and localizer images is performed differently.

** Segmentation of axial images **

Segmentation of axial images is performed by edge detection.

1. Before edge detection, the image data array is expanded in the horizontal direction allowing some space around the body contour in case the image border cuts the view of the the patient. This expansion is later removed before final masking.

2. The image pixel values is also ranked, i.e. assigned unique values in the corresponding magnitude order. This yields a matrix with sequential positive values.

3. A Canny filter detects the edges. First it performs a smoothing of the image and then it evaluates the differences between adjacent pixels. It also combines weak edges with strong edges to create coherent edges.

4. Creases and blurriness sometimes creates small interruptions in the detected edge.
If, at first, the edge detection does not yield closed objects an intermediate step is performed by dilating the detected edge by binary dilation. This dilation is later removed after the mask is created.

5. Edges that form closed objects are filled.
Filled objects with area less than an arbitrary threshold, 15% of all pixels, is removed.
This removes the table from the image.

** Segmentation of localizer images **

Seqmentation of localizer images are performed by a sliding threshold algorithm.
The sliding threshold algorithm threshold the pixels within a defined range of pixel values.
The speed of which the ratio of threshold pixels compared to the total number of pixels is a measure of the number of pixels with similar pixel values in the image.
Homogeneous areas, such as the surrounding air and the table top, consists of large number of pixels with similar pixel values regardless of how different manufacturers have chosen to implement their routines.
The peaks in the derivative ratio of thresholded pixels indicates pixel values of air and the table top.

1. Manufacturer specific ranges and offsets to the peaks are set accordingly.
   Currently, specific ranges has been set for
   GE LightSpeed VCT,
   Philips Brilliance Big Bore, and
   Siemens Somatom Definition Flash.

2. Thresholding is performed over the chosen range and the ratio of thresholded pixels are recorded.

3. The approriate thresholding level is found at the base of a manufacturer specific peak in the derivative ratio curve.

4. The crude thresholded mask is dilated end eroded to close small gaps before the blobs are filled.
Blobs smaller than an arbitrary mask size limit are removed.

5. A contour around the mask is created.


The contour and mask is stored in the image object.

### Patient size calculation ###

Patient size calculation is performed in a number of different ways, suggested by
AAPM Report 204 [(pdf)](https://www.aapm.org/pubs/reports/RPT_204.pdf) and
AAPM Report 220 [(pdf)](https://www.aapm.org/pubs/reports/RPT_220.pdf).

The conversion factors (CF) is then calculated for both the 16 cm phantom and 32 cm fantom,
according to the AAPM Reports 204 and 220, using the available variants of effectiv diamter (ED) estimation variants
as well as AP or LAT estimate.

The patient offset from isocenter is also calculated.
This calculation is performed on the axial image.
The necessary dicom tag is present in the files from the Siemens Somatom Definition Flash
but seem to be missing in those from Philips Brilliance Big Bore and GE LightSpeed VCT.


** Patient size from axial images  **

* AP and LAT from the image mask.
The vertical distance between the topmost mask pixel and the lowest pixel, in cm via the pixel spacing dicom tag.
* AP+LAT, from AP and LAT.
* The elliptical effective diameter (EED), calculated from the AP and LAT values according to AAPM Report 204.
* The area equivalent diameter (AED),
as the diameter of a cylinder of equal area to that of the patient's cross section on the CT image.
* The circumference of the patient, as the circumference of the above cylinder of equal area.
* The water equivalent diamenter, WED, from the Hounsfield values in the image data contained within the masked contour.
* AP and LAT is currently also estimated using the sinogram of the Radon transformed mask.
This method also estimates the patient axial rotation.

![PatientSize_AAPM_Report_204.PNG](https://bitbucket.org/repo/kXzLqE/images/4161434491-PatientSize_AAPM_Report_204.PNG "Patient Size, AAPM Report 204")

** Patient size from localizer images  **

* AP and LAT from the image mask of the corresponding sideview and frontview, respectively.
As the crossection of the image mask for every transectional pixel row, in cm via the pixel spacing dicom tag.
The AP or LAT value for the image midpoint is singled out
but all values are stored as an array with corresponding z-position in mm.
* The water equivalent diameter, as evaluated from the localizer radiograph.
A variant of the suggestion from Menke 2005 [(link)](http://dx.doi.org/10.1148/radiol.2362041327).
Manufacturer specific pixel values for air and water has to be predefined
as the pixel values of localizer images do not represent Hounsfield values.
Also, the localizer WED is evaluated using a one pixel width ROI
instead of the ROI covering the exam area, as suggested by Menke.

### Integration with [OpenREM](http://openrem.org/) ###

This program is intended to have integration with OpenREM.
Co-operation with the developers of the OpenREM software has been initiated.

Later work will consist of:

* Writing tests
* Code review
* Other guidelines

### Program development ###

Which tags of the DICOM tags to store in the model is determined by the list variable *extraTags* found in the beginning of the *dcmImportCT.py* module.
Adding tags to store is done by adding tags to *extraTags*.
Storing those added tags in the model is done by adding corresponding variables to the *Image* class found in the *dcmImportCT.py* module.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact