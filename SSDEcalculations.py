"""
    GPL-3.0

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016 Morgan Nyberg

    This file is part of OpenSSDE.

    OpenSSDE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenSSDE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenSSDE.  If not, see <http://www.gnu.org/licenses/>.
"""
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy import ndimage
from skimage.feature import canny  # for segmenteraAxial and segmenteraScoutCanny
from skimage.filters import rank_order  # for segmenteraAxial and segmenteraScoutCanny
from skimage.transform import radon
from skimage.draw import circle_perimeter
from scipy.ndimage.measurements import center_of_mass
from conversionFunctions import *
from PyQt4 import QtGui  # for showing a thumbnail image in the results tab
from SSDEexport import saveLocalizerData  # used in function estimateScout
from SSDEexport import saveMenke2005Data  # used in function estimateScout


def isocenter_deviation(currentImage=None):
    """ Distance between isocenter and image center / patient center. Not yet connected to the rest of the program.

    The isocenter is normally located at the center of the image.
    If it isn't, then the CT software automatically translates the image coordinates to put it so that the
    isocenter is located in the middle of the image (ReconstructionTargetCenterPatient).
    But we are interested in whether the patient was located in the isocenter at the time of imaging.
    The TableHeight is chosen/calibrated to place the image center in the middle of the image.
    If the TableHeight, or DataCollectionCenterPatient, is not the same as the ReconstructionTargetCenterPatient,
    then the patient has been offset during imaging.

    Furthermore, the patient geometrical center might not be at the center of the image
    and this would cause yet another offset.

    This function calculates the isocenter offset of the image and the patient geometrical center in mm.

    Usage:
        isocenter_deviation(self.currentImage)

    Uses:

    Used by:
        SSDEmain.py->MainW.on_calcButton_released()

    :param currentImage:
    :return:
    """
    dcc = currentImage.DataCollectionCenter
    rtc = currentImage.ReconstructionTargetCenter  # assumed to indicate the true isocenter location
    th = currentImage.tableHeight
    print('DataCollectionCenter: ' + str(dcc))
    print('ReconstructionTargetCenter: ' + str(rtc))
    print('Table Height: ' + str(th))

    image = currentImage.imageslice.copy()
    imageXsize_px, imageYsize_px = image.shape
    imageXcenter = imageXsize_px / 2.0
    imageYcenter = imageYsize_px / 2.0
    imageXsize_mm = imageXsize_px * currentImage.pixelspacing[0]
    imageYsize_mm = imageYsize_px * currentImage.pixelspacing[1]
    #  print('Image size X = {0:.1f} mm, Y = {0:.1f} mm'.format(imageXsize_mm, imageYsize_mm))
    print('Image size:')
    print('X (mm) = ' + str(imageXsize_mm))
    print('Y (mm) = ' + str(imageYsize_mm))
    if currentImage.geometrical_centre:
        gc = currentImage.geometrical_centre[0] * currentImage.pixelspacing[0], currentImage.geometrical_centre[1] * \
             currentImage.pixelspacing[1]
        #  print(currentImage.pixelspacing[0])
        print('patient geometrical center, from upper left corner (mm)')
        print('X = {0:.1f} mm, Y = {0:.1f} mm'.format(gc[0], gc[1]))
        diff_gm_Xcenter_px = currentImage.geometrical_centre[0] - imageXcenter
        diff_gm_Ycenter_px = currentImage.geometrical_centre[1] - imageYcenter
        diff_gm_Xcenter_mm = diff_gm_Xcenter_px * currentImage.pixelspacing[0]
        diff_gm_Ycenter_mm = diff_gm_Ycenter_px * currentImage.pixelspacing[1]
        print('patient geometrical center offset from image mid,')
        print('X = {0:.1f} mm, Y = {0:.1f} mm'.format(diff_gm_Xcenter_mm, diff_gm_Ycenter_mm))
        # calculate offset of patient relative image midpoint/isocenter/DataCollectionCenterPatient [mm]
        try:
            #  print('dcc x {}, y {}'.format(dcc[0], dcc[1]))
            #  print('dcc x {}, y {}'.format(rtc[0], rtc[1]))
            #  diff_table_X_mm = float(rtc[0]) - float(dcc[0])
            #  diff_table_Y_mm = float(rtc[1]) - float(dcc[1])
            rtc_x = float(rtc[0])
            rtc_y = float(rtc[1])
            dcc_x = float(dcc[0])
            dcc_y = float(dcc[1])
            print('dcc x {}, y {}'.format(dcc_x, dcc_y))
            print('rtc x {}, y {}'.format(rtc_x, rtc_y))
            diff_table_X_mm = rtc_x - dcc_x
            diff_table_Y_mm = rtc_y - dcc_y
            #  print('diff rtc-dcc: X = {0:.1f} mm, Y = {0:.1f} mm'.format(diff_table_X_mm, diff_table_Y_mm))
            print('diff_table_X_mm:')
            print(diff_table_X_mm)
            print('diff_table_Y_mm:')
            print(diff_table_Y_mm)
            X_offset_mm = diff_table_X_mm - diff_gm_Xcenter_mm
            Y_offset_mm = diff_table_Y_mm - diff_gm_Ycenter_mm
            currentImage.patient_offset = X_offset_mm, Y_offset_mm
            #  print('Total offset from isocenter: X = {0:.1f} mm, Y = {0:.1f} mm'.format(X_offset_mm, Y_offset_mm))
            print('Total offset from isocenter:')
            print('X (mm):')
            print(X_offset_mm)
            print('Y (mm):')
            print(Y_offset_mm)
        except TypeError:
            print('TypeError')
            print('DataCollectionCenter:')
            print(dcc)
            print('ReconstructionTargetCenter:')
            print(rtc)

    imageposition = currentImage.imageLocation
    pixel_size = currentImage.pixelspacing
    plt.imshow(image, cmap='bone', interpolation='nearest')
    plt.show()


def setBusyLamp(tab=None, busy=False):
    """ Set an image in the GUI to show if the program is busy with something

    Set an image in the GUI to show if the program is busy with something
    in those cases which the progress bar cannot be of use.

    Usage: setBusyLamp(self, True)

    Uses:

    Used by: Segment and Calculate functions

    :param buzy: True if busy, False if not busy
    :return: Changes the busy image in the GUI accordingly
    """

    # choose which image to show
    if busy:
        CurrentStatusQimage = ':\\busy_red.png'
    else:
        CurrentStatusQimage = ':\\busy_lightgray.png'

    # Show the image in the buzy label
    CurrentStatusPixmap = QtGui.QPixmap(CurrentStatusQimage)
    tab.label_busy.setPixmap(CurrentStatusPixmap)
    tab.label_busy.show()


def calculateEquivDiameter_2016_04_27(currentImage=None):
    """ Calculate the equivalent diameter and circumference.

    Calculates the equivalent diameter and circumference of the patient (only for axial images).


    :param currentImage:
    :return:
    """
    pixel_size = currentImage.pixelspacing  # x- and y-distance between pixels (in mm according to the standard)
    mask = np.zeros_like(currentImage.imagemask)  # make a zero-filled array
    mask[currentImage.imagemask] = 1  # set the mask pixels to one where tho body is
    plt.imshow(mask, cmap='bone', interpolation='nearest')
    plt.show()
    mask_area_px = np.sum(mask)  # count the number of pixels in the mask, corresponds to the area (in pixels)
    # A = PI * d^2 /4  -> d^2 = 4A/PI -> d = 2*SQRT(A/PI)
    # O = PI * d -> PI * 2 * SQRT(A)/SQRT(PI) -> 2*SQRT(A*PI)
    mask_eqv_diameter_px = 2.0*math.sqrt(mask_area_px/math.pi)
    mask_eqv_diameter_cm = 2.0*math.sqrt(mask_area_px/math.pi)*currentImage.pixelspacing[0]/10.0
    mask_eqv_circumference_px = 2.0*math.sqrt(mask_area_px*math.pi)
    mask_eqv_circumference_cm = 2.0*math.sqrt(mask_area_px*math.pi)*currentImage.pixelspacing[0]/10.0
    print('Ekvivalent diameter (px): ' + str(mask_eqv_diameter_px))
    print(pixel_size)
    print('Ekvivalent diameter (cm): {:.2f}'.format(mask_eqv_diameter_cm))  # .1 egentligen men .2 medan man testar
    print('Ekvivalent omkrets (px): ' + str(mask_eqv_circumference_px))
    print('Ekvivalent omkrets (cm): {:.2f}'.format(mask_eqv_circumference_cm))

    # summera masken radvis respektive kolumnvis
    collapse_rows_lat = np.sum(mask,axis=0)
    lat_px = np.count_nonzero(collapse_rows_lat)
    lat_cm = lat_px*currentImage.pixelspacing[0]/10.0
    collapse_columns_ap = np.sum(mask,axis=1)
    ap_px = np.count_nonzero(collapse_columns_ap)
    ap_cm = ap_px*currentImage.pixelspacing[1]/10.0
    print('LAT (px): ' + str(lat_px))
    print('AP (px): ' + str(ap_px))
    print('LAT (cm): {:.2f}'.format(lat_cm))  # .1 egentligen men .2 medan man testar
    print('AP (cm): {:.2f}'.format(ap_cm))

    plt.plot(collapse_rows_lat,'r--')    #  bredare -> = LAT
    plt.plot(collapse_columns_ap,'b--')  # mindre  -> = AP
    plt.show()

    mask_radon = radon(mask)
    radon_max_px = 0
    radon_max_degree = 0
    radon_min_px = np.size(mask_radon,axis=0)
    # print(radon_min_px)
    radon_min_degree = 0
    for rot_degree in range(0,180):
        non_zero_px = np.count_nonzero(mask_radon[:, rot_degree])
        if non_zero_px>radon_max_px:
            radon_max_px = non_zero_px
            radon_max_degree = rot_degree
        if non_zero_px<radon_min_px:
            radon_min_px = non_zero_px
            radon_min_degree = rot_degree
    print('Radon max (px): ' + str(radon_max_px) + ' @ ' + str(radon_max_degree) + ' degrees')
    print('Radon min (px): ' + str(radon_min_px) + ' @ ' + str(radon_min_degree) + ' degrees')
    plt.imshow(mask_radon, cmap='bone', interpolation='nearest')
    plt.show()


    # calculate average HU of  the image slice (/pixel)
    bilddata = currentImage.imageslice
    image = bilddata.copy()
    image[~mask] = 0  # set HU to zero outside the body
    HUsum_px = np.sum(image)
    HUaverage = HUsum_px/mask_area_px
    print('Average slice HU, per pixel: {:.1f}'.format(HUaverage))
    plt.imshow(image, cmap='bone', interpolation='nearest')
    plt.show()


def showWED(view, currentImage=None):
    """ Shows the image and the WED, as a horizontal line, of the chosen image object in the GUI

    Usage:
        showContour(self.graphicsView, self.currentImage)

    Uses:

    Used by:
        SSDEmain.py->MainW.on_calcButton_released()

    :param view:
    :param currentImage:
    :return:
    """
    view.level = [currentImage.windowCenter, currentImage.windowWidth]
    bildlista = []
    bildlista.append(currentImage.imageslice)
    positionslista = []
    positionslista.append(currentImage.pos)
    # add the countour and the mask
    #  bildlista.append(currentImage.imagecontour)
    #  positionslista.append(currentImage.pos)
    #  bildlista.append(currentImage.imagemask)
    #  positionslista.append(currentImage.pos)
    # draw the contour on the image
    bilddata = currentImage.imageslice
    contoured_Image = bilddata.copy()
    contoured_Image[currentImage.imagecontour] = 2000  # colour the edge with high HU
    bildlista.append(contoured_Image)
    positionslista.append(currentImage.pos)

    masked_Image = bilddata.copy()
    HUmin = bilddata.min()
    masked_Image[~currentImage.imagemask] = HUmin  # set a low HU outside the body
    bildlista.append(masked_Image)
    positionslista.append(currentImage.pos)

    bilddata = currentImage.imageslice
    HUmin = bilddata.min()
    HUmax = bilddata.max()
    WED_Image = bilddata.copy()
    WED_Image[~currentImage.imagemask] = HUmin  # set a low HU outside the body
    CM_r = int(round(currentImage.geometrical_centre[0]))
    CM_c = int(round(currentImage.geometrical_centre[1]))
    # circle with corresponding area
    mask_cicle_perimeter = circle_perimeter(CM_r, CM_c, int(round(0.5*currentImage.equal_area_diameter_px)))
    WED_Image[mask_cicle_perimeter] = HUmax
    # Line corresponding to WED through the CM
    L_r = CM_c-int(round(0.5*currentImage.WED_px))
    L_l = CM_c+int(round(0.5*currentImage.WED_px))
    WED_Image[CM_r-1:CM_r+1,L_r:L_l] = HUmax
    bildlista.append(WED_Image)
    positionslista.append(currentImage.pos)


    # send them all to the view
    view.setNumpyArrays(bildlista, positionslista)


def showContour(view,currentImage=None):
    """ Shows the image and image contour of the chosen image object in the GUI

    Usage:
        showContour(self.graphicsView, self.currentImage)

    Uses:
        displayView.py->ImageView.setNumpyArrays()

    Used by:
        SSDEmain.py->MainW.on_segmentButton_released()

    :param view:
    :param currentImage:
    :return:
    """

    # show the image in the GUI
    # send 'bilddata'/chosenItem.imageslice (numpy-array) to 'ImageView.setNumpyArrays' in displayView.py
    # but first set the windowing level stored in the dicom file
    view.level = [currentImage.windowCenter, currentImage.windowWidth]
    # ImageView.setNumpyArrays(array, pos)  # expects a list, lets send a list of lists
    bildlista = []
    bildlista.append(currentImage.imageslice)
    positionslista = []
    positionslista.append(currentImage.pos)
    bilddata = currentImage.imageslice
    contoured_Image = bilddata.copy()
    contoured_Image[currentImage.imagecontour] = 2000  # colour the edge with high HU
    bildlista.append(contoured_Image)
    positionslista.append(currentImage.pos)

    masked_Image = bilddata.copy()
    HUmin = bilddata.min()
    masked_Image[~currentImage.imagemask] = HUmin  # set a low HU outside the body
    bildlista.append(masked_Image)
    positionslista.append(currentImage.pos)
    view.setNumpyArrays(bildlista, positionslista)


def showSlice(view,currentImage=None):
    """ Shows the image of the chosen image object in the GUI

    Usage:
        showSlice(self.graphicsView, self.currentImage)

    Uses:
        displayView.py->ImageView.setNumpyArrays()

    Used by:
        SSDEmain.py->MainW.on_chooseImageButton_released()

    :param view:
    :param currentImage:
    :return:
    """
    # show the image in the GUI
    # send 'bilddata'/chosenItem.imageslice (numpy-array) to 'ImageView.setNumpyArrays' in displayView.py
    # but first set the windowing level stored in the dicom file
    # self.graphicsView.level = [currentImage.windowCenter, currentImage.windowWidth]
    view.level = [currentImage.windowCenter, currentImage.windowWidth]
    # ImageView.setNumpyArrays(array, pos)  # expects a list, lets send a list of lists
    bildlista = []
    bildlista.append(currentImage.imageslice)
    positionslista = []
    positionslista.append(currentImage.pos)
    view.setNumpyArrays(bildlista, positionslista)


def segmenteraScout(currentImage=None):
    """ Finds the body contour in a scout CT image.

    Finds the body contour, and creates a mask covering the body, in a scout CT image
    by a sliding threshold algorithm.

    The sliding threshold algorithm threshold the pixels within a defined range of pixel values.
    The speed of which the ratio of threshold pixels compared to the total number of pixels is a measure of
    the number of pixels with similar pixel values in the image.
    Homogenous areas, such as the surrounding air and the table top, consists of large number of pixels
    with similar pixel values regardless of how different manufacturers have chosen to implement their routines.
    The peaks in the derivative ratio of thresholded pixels indicates pixel values of air and the table top.

    Manufacturer specific ranges  and offsets to the peaks are set accordingly.

    The crude thresholded mask is dilated end eroded to close small gaps befor the blobs are filled.
    Blobs smaller than an arbitrary mask size limit are removed.

    A contour around the mask is created.

    The contour and mask is stored in the image object.

    Usage:
        segmenteraScout(self.currentImage)

    Uses:
        numpy->np.min
        numpy->np.max
        numpy->np.empty_like
        numpy->np.ones_like
        numpy->np.sum
        numpy->np.convolve
        numpy->np.bincount
        scipy->ndimage.binary_erosion
        scipy->ndimage.morphology.binary_dilation
        scipy->ndimage.label
        SSDEcalculations.py->close_small_gaps

    Used by:
        SSDEmain.py

    :param currentImage: Image-object
    :return: sets Image.mask_success, Image.imagecontour and Image.imagemask
    """

    mask_success = False  # initiera standardvärdet
    bilddata = currentImage.imageslice
    Xshape, Yshape = bilddata.shape
    image_size = Xshape*Yshape
    # maska bort allt som fyller mindre än 5% av bilden (10%, 15%)
    mask_size_limit = 0.05*image_size  # 5% to allow for phantoms

    HUmin = np.min(bilddata)
    HUmax = np.max(bilddata)
    #print('Humin = ' + str(HUmin) + ', HUmax = ' + str(HUmax))
    Wmin = currentImage.windowCenter - currentImage.windowWidth/2.0
    Wmax = currentImage.windowCenter + currentImage.windowWidth/2.0
    #print('Wmin = ' + str(Wmin) + ', Wmax = ' + str(Wmax))

    plt.imshow(bilddata, cmap='bone', interpolation='nearest', clim=(Wmin-100, Wmax+20))
    print('image')
    plt.show()

    # Set up a standard range to slide the window over, and a standard width
    WC = range(-1000, 100)
    print(currentImage.manufacturer)
    print(currentImage.model)
    print(currentImage.scoutview)
    print(currentImage.imageOrientation)

    # Update the window range for known systems
    if currentImage.manufacturer == 'GE MEDICAL SYSTEMS':
        WC = range(-500, 50)
    if currentImage.manufacturer == 'SIEMENS':
        WC = range(-300, 20)
    WWnr = 1
    # create an array to store the results
    # ratio is the ratio of the number of thresholded pixels
    ratiolist = np.empty_like(WC, dtype=float)
    dWCstep = 1.0
    for i in range(len(WC)):
        for j in range(WWnr):
            Wthreshold = WC[i]
            bildkopia = bilddata.copy()
            bildmask = np.ones_like(bilddata)
            bildmask[bildkopia < Wthreshold] = 0
            mask_size = np.sum(bildmask)
            mask_ratio = mask_size/image_size
            ratiolist[i] = mask_ratio
            bildmask[0, 0] = 0  # make sure that at least one pixel has value 0, for imshow

    # calculate the derivative of the ratio curve, to detect steep slopes as peaks
    conv_ratio = np.convolve(ratiolist, [1, -1], 'same')/dWCstep  # 'same' to avoid off-by-one array
    # find the location of  the first peak
    # use an offset to avoid any peak at the start of the curve
    conv_offset = 10
    min_d_i = conv_ratio[conv_offset:].argmin() + conv_offset  # find index of minimum value, to get the WC
    WC_i = min_d_i   # standard assignment of WC_i
    WC_choice = WC[WC_i]

    # manufacturer specific choice of threshold
    # the 'base' of the peak will be used as threshold
    if currentImage.manufacturer == 'GE MEDICAL SYSTEMS' and not currentImage.sideview:
        WC_i = min_d_i+25
        WC_choice = WC[WC_i]  # + 25
        print('Frontal Lightspeed')
    if currentImage.manufacturer == 'GE MEDICAL SYSTEMS' and currentImage.sideview:
        print('Sidovy Lightspeed')
        # Find the next peak for this one
        conv_offset = min_d_i+30
        min_d_i = conv_ratio[conv_offset:].argmin() + conv_offset
        WC_i = min_d_i + 40
        WC_choice = WC[WC_i]
    if currentImage.manufacturer == 'Philips':
        WC_i = min_d_i + 75
        WC_choice = WC[WC_i]
        print('Philips')
    if currentImage.manufacturer == 'SIEMENS':
        WC_i = min_d_i + 10
        WC_choice = WC[WC_i]
        print('Flash / Topogram')
    final_ratio_choice = ratiolist[WC_i]
    print('final choice ratio: ' + str(final_ratio_choice))
    print('WC val: ' + str(WC_choice))

    # make the mask at the found location
    print('make the mask at the found location')
    Wthreshold = WC_choice
    bildkopia = bilddata.copy()
    bildmask = np.ones_like(bilddata)
    bildmask[bildkopia < Wthreshold] = 0
    plt.imshow(bildmask, cmap='bone', interpolation='nearest')
    plt.show()

    # clean small stuff by erode and dilate
    eroded_image = ndimage.binary_erosion(bildmask, iterations=2)
    bildmask = ndimage.morphology.binary_dilation(eroded_image, iterations=2)

    # preclean larger blobs
    # skip preclean when final_ratio_choice is low as this indicate phantom instead of patient
    ProbablyPatient_NotPhantom = final_ratio_choice > 0.25
    if ProbablyPatient_NotPhantom:
        label_objects, nb_labels = ndimage.label(bildmask)
        sizes = np.bincount(label_objects.ravel())
        mask_sizes = sizes > mask_size_limit
        mask_sizes[0] = 0
        bildmask = mask_sizes[label_objects]
        print('image_precleaned')
    else:
        print('image not precleaned, probably phantom')

    # close small stuff by dilate and erode
    # try this with two strengths but avoid to connect 'table blobs'
    filled_image = close_small_gaps(bildmask)

    # remove smaller objects than the mask_size_imit
    # label the connected blobs in the binary image mask
    # then measure their sizes and erase the ones smaller than the chosen size limit
    # lastly, check whether there remains a mask or completely empty image mask
    label_objects, nb_labels = ndimage.label(filled_image)
    sizes = np.bincount(label_objects.ravel())
    mask_sizes = sizes > mask_size_limit
    mask_sizes[0] = 0
    ImageMask = mask_sizes[label_objects]
    print('image_cleaned')
    # check if there is any mask
    mask_success = not(np.sum(ImageMask) == 0)
    # should stop if not mask success

    # construct a contour around the mask
    edge_ImageMask = ImageMask.copy()
    dilated_ImageMask = ndimage.morphology.binary_dilation(ImageMask, iterations=1)
    edge_ImageMask = dilated_ImageMask - edge_ImageMask

    # mask the image with the edge of the mask
    edge_MaskedImage = bilddata.copy()
    edge_MaskedImage[edge_ImageMask] = 1200  # colour the edge with high HU
    plt.imshow(edge_MaskedImage, cmap='bone')
    plt.show()

    # Mask the original image, just for showing
    MaskedImage = bilddata.copy()
    MaskedImage[~ImageMask] = HUmin  # set a low value (dark) outside masked body
    plt.imshow(MaskedImage, cmap='bone')
    plt.show()

    if mask_success:
        currentImage.mask_success = True
        currentImage.imagecontour = edge_ImageMask  # edge_MaskedImage
        currentImage.imagemask = ImageMask  # MaskedImage
        print('Mask end contour stored.')
    else:
        print('Could not mask')


def close_small_gaps(image_mask):
    """ close small gaps and kinks by dilate and erode

    Clean up a binary image mask by closing small gaps and kinks using dilation follow by same amount of erosion.

    This might connect blobs in an unwanted fashion so this process is performed with two strengths.
    Uses the more gentle process if the more aggressive dilation and erosion process connects separate structure.
    The algorithm decides by performing a cleaning of smaller blobs and compares the resulting masks.
    If unwanted connections has arised then the difference between the image mask sizes will be considerate.

    Usage:
        filled_image = close_small_gaps(bildmask)

    Uses:
        numpy->np.sum
        scipy->ndimage.binary_fill_holes
        scipy->ndimage.morphology.binary_dilation
        scipy->ndimage.binary_erosion

    Used by:
        SSDEcalculations.py->segmenteraScout

    :param image_mask: binary image mask to close small gaps and kinks in
    :return: image mask with small gaps and kinks closed
    """
    # close small stuff by dilate and erode
    # try this with two strengths but avoid to connect 'table blobs'

    Xshape, Yshape = image_mask.shape
    image_size = Xshape*Yshape

    dilation_size = 1
    dilated_bildmask1 = ndimage.morphology.binary_dilation(image_mask, iterations=dilation_size)
    fill_image1 = ndimage.binary_fill_holes(dilated_bildmask1)
    eroded_fill_image1 = ndimage.binary_erosion(fill_image1, iterations=dilation_size)

    dilation_size = 3
    dilated_bildmask2 = ndimage.morphology.binary_dilation(image_mask, iterations=dilation_size)
    fill_image2 = ndimage.binary_fill_holes(dilated_bildmask2)
    eroded_fill_image2 = ndimage.binary_erosion(fill_image2, iterations=dilation_size)

    fill_image_diff = eroded_fill_image2-eroded_fill_image1
    fill_image_diff_sum = np.sum(fill_image_diff)
    print('diff = ' + str(fill_image_diff_sum) + ' ratio: ' + str(fill_image_diff_sum/image_size))

    # if the mask suddenly became much larger it probably connected the patient mask with a 'table blob'
    if fill_image_diff_sum <= 0.01*image_size:
        filled_image = eroded_fill_image2
        print('dil2')
    else:
        filled_image = eroded_fill_image1
        print('dil1')

    return filled_image


def segmenteraAxial(currentImage=None):
    """ Finds the body contour in an axial CT image.

    Finds the body contour, and creates a mask covering the body, in an axial CT image
    by the Canny edge detection algorithm.

    Before edge detection, the image data array is expanded in the horisontal direction
    allowing some space around the body contour in case the image border cuts the view of the the patient.
    his expansion is later removed before final masking.

    The image pixel values is also ranked, i.e. assigned unique values in the corresponding magnitude order.
    This yields a matrix with sequential positive values.

    A Canny filter detects the edges.
    First it performs a smoothing of the image and then it evaluates the differences between adjacent pixels.
    It also combines weak edges with strong edges to create coherent edges.

    Creases and blurriness sometimes creates small interruptions in the detected edge.
    If, at first, the edge detection does not yield closed objects an intermediate step is performed by
    dilating the detected edge by binary dilation.
    This dilation is later removed after the mask is created.

    Edges that form closed objects are filled.
    Filled objects with area less than an arbitrary threshold, 15% of all pixels, is removed.
    This removes the table from the image.

    The image contour and mask are stored in the image object.

    Usage:
        segmenteraAxial(self.currentImage)

    Uses:
        skimage.feature->canny,
        skimage.filters->rank_order
        numpy->np.ones
        numpy->np.concatenate
        numpy->np.bincount
        numpy->np.any
        scipy->ndimage.binary_fill_holes
        scipy->ndimage.morphology.binary_dilation
        scipy->ndimage.binary_erosion
        scipy->ndimage.label

    Used by:
        SSDEmain.py

    :param currentImage: Image-object
    :return: sets Image.mask_success, Image.imagecontour and Image.imagemask
    """

    mask_success = False  #

    image = currentImage.imageslice
    plt.imshow(image, cmap='bone', interpolation='nearest')
    plt.show()

    # Add a frame of 'background colour' to handle 'clipped patients', but only horizontally, not vertically
    frame_size = 10
    HUmin = image.min()
    Xshape, Yshape = image.shape
    # mask_size_limit is a measure of smallish stuff to clean away from the image (table remnants)
    # it is suitable to calculate this here
    mask_size_limit = Xshape*Yshape*0.15

    x_frame = HUmin*np.ones([Xshape, frame_size])
    framed_image = np.concatenate((x_frame, image), axis=1)
    framed_image = np.concatenate((framed_image, x_frame), axis=1)

    # create an image with ranked pixel values before Canny edge detection
    rank_image, original_values = rank_order(framed_image)
    # edge detection with Canny filter
    rank_image_max = rank_image.max()
    low_thres = 0.1*rank_image_max  # Canny filter standard value
    high_thres = 0.2*rank_image_max  # Canny filter standard value
    suddgrad = 1.4  # 1.4 seems to be a suitable value
    edges = canny(rank_image, sigma=suddgrad, low_threshold=low_thres, high_threshold=high_thres)

    fill_image = ndimage.binary_fill_holes(edges)
    # label the connected blobs in the binary image mask
    # then measure their sizes and erase the ones smaller than the chosen size limit
    # lastly, check whether there remains a mask or completely empty image mask
    label_objects, nb_labels = ndimage.label(fill_image)
    sizes = np.bincount(label_objects.ravel())
    mask_sizes = sizes > mask_size_limit
    mask_sizes[0] = 0
    image_cleaned = mask_sizes[label_objects]
    # check if any mask was created
    if np.any(image_cleaned):
        mask_success = True
        print('Found the contour during the first try.')
    else:
        print('Could not mask the contour. Try dilation')
        # dilate the edges (erode the filled image correspondingly right after)
        dilated_edges = ndimage.morphology.binary_dilation(edges, iterations=1)
        fill_image = ndimage.binary_fill_holes(dilated_edges)
        # erode
        eroded_fill_image = ndimage.binary_erosion(fill_image, iterations=1)
        # label the connected blobs in the binary image mask
        # then measure their sizes and erase the ones smaller than the chosen size limit
        # lastly, check whether there remains a mask or completely empty image mask
        label_objects, nb_labels = ndimage.label(eroded_fill_image)
        sizes = np.bincount(label_objects.ravel())
        mask_sizes = sizes > mask_size_limit
        mask_sizes[0] = 0
        image_cleaned = mask_sizes[label_objects]
        if np.any(image_cleaned):
            mask_success = True
            print('Success with dilation.')
        else:
            print('No succes with dilation either.')

    # Remove the 'horizontal frame'  for the mask
    transposedFramedMask = image_cleaned.transpose()
    Xshape, Yshape = transposedFramedMask.shape
    transposedClippedFramedMask = transposedFramedMask[frame_size:Xshape-frame_size]
    ClippedFramedImageMask = transposedClippedFramedMask.transpose()  # boolean mask
    # remember that this image was only framed in the horizontal direction
    # had it been framed in the vertical direction also, then that frame would have to be removed also

    # Erode one pixel from the mask, noticed this when using Catphan
    # eroded_image_mask = ndimage.binary_erosion(ClippedFramedImageMask, iterations=1)
    # Don't, as noticed after having measured the phantom
    eroded_image_mask = ClippedFramedImageMask

    ImageMask = eroded_image_mask.copy()  # '~' not '.invert' for boolean
    plt.imshow(ImageMask, cmap='bone', interpolation='nearest')
    plt.show()

    # extract the image mask edge
    edge_ImageMask = ImageMask.copy()
    dilated_ImageMask = ndimage.morphology.binary_dilation(ImageMask, iterations=1)
    edge_ImageMask[~dilated_ImageMask] = HUmin
    edge_ImageMask = ~edge_ImageMask  # '~' not '.invert' for boolean
    # mask the image with the edge of the mask
    edge_MaskedImage = image.copy()

    edge_MaskedImage[edge_ImageMask] = 1200  # colour the edge with high HU
    currentImage.imagecontour = edge_MaskedImage
    plt.imshow(edge_MaskedImage, cmap='bone', interpolation='nearest')
    plt.show()

    # Mask a copy of the original image, to show
    MaskedImage = image.copy()
    MaskedImage[~ImageMask] = HUmin  # set a low pixel value outside the masked body
    currentImage.imagemask = MaskedImage
    plt.imshow(MaskedImage, cmap='bone', interpolation='nearest')
    plt.show()

    # store the body contour and mask in the Image object, if successfully found
    if mask_success:
        currentImage.mask_success = True
        currentImage.imagecontour = edge_ImageMask  # edge_MaskedImage
        currentImage.imagemask = ImageMask  # MaskedImage
        print('Store mask end contour.')
        patientClipped(currentImage)
    else:
        print('No mask or contour stored.')
        # pass


def patientClipped(currentImage=None):
    """ Determine if the patient body contour is clipped by the image frame

    Determines if the patient body contour is clipped by the image frame and sets
    the variable patient_clipped in the Image object to True or False.

    The side columns of the image mask is evaluated, and if there are more than a few pixels
    present then it is assumed that the patent body would extend beyond the reconstructed image frame.

    Usage:
        patientClipped(currentImage)

    Uses:
        numpy.py->np.zeros_like
        numpy.py->np.sum

    Used by:
    SSDEcalculations.py->segmenteraAxial()

    :param currentImage:
    :return: True or False, and sets the variable patient_clipped in the Image object to True or False
    """
    if not currentImage.mask_success:
        print("Image has no mask.")
        return

    edge_pixel_limit = 6
    mask_image = np.zeros_like(currentImage.imagemask)
    mask_image[currentImage.imagemask] = 1
    edge_pixels_l = mask_image[:,0]
    edge_pixels_r = mask_image[:,-1]
    sum_edge_pixels_l = np.sum(edge_pixels_l)
    sum_edge_pixels_r = np.sum(edge_pixels_r)
    print("edge_pixels_l: " + str(sum_edge_pixels_l))
    print("edge_pixels_r: " + str(sum_edge_pixels_r))
    if sum_edge_pixels_l > edge_pixel_limit or sum_edge_pixels_r > edge_pixel_limit:
        print("More than a few pixels detected at the edge")
        result = True
    else:
        print("Patient not likely clipped")
        result = False

    currentImage.patient_clipped = result
    return result


def calculateEquivDiameter(currentImage=None):
    """ Calculate the patient size for axial images

    This function is implemented for axial images.
    It calculates:
     - the area equivalent diameter and circumference of the patient,
     - the mean and median HU-value inside the masked body
     - the WED according to AAPM Report 220 §2.1
     - the lateral (LAT) and posteriaterio (AP) measure of the patient, and AP+LAT
     - the elliptical, effective diameter according to AAPM Report 204

    The function also illustrates the WED as a diameter drawn through the body slice center of mass
    as well as the area equivalent diameter as a circle around the body slice center of mass.

    Currently, the function also performs a Radon transformation and finds, in the resulting sinogram,
    the largest and smallest patient width at their respective rotational degrees.
    This could be used to discern whether the patient is positioned flat at the bed or rotated.

    The patient size values are stored in the image object.

    Usage:
        calculateEquivDiameter(self.currentImage)

    Uses:
        numpy->np.zeros_like
        numpy->np.sum
        numpy->np.mean
        numpy->np.median
        numpy->np.count_nonzero
        numpy->np.size
        math->math.sqrt
        math->math.pi
        scipy.ndimage.measurements->center_of_mass
        skimage.draw->circle_perimeter
        skimage.transform->radon

    Used by:
        SSDEmain.py


    :param currentImage:
    :return: Sets the corresponding values for patient size in the Image object (currentImage)
    """
    pixel_size = currentImage.pixelspacing  # x- and y-distance between pixels (in mm according to the standard)
    mask = np.zeros_like(currentImage.imagemask)  # make a zero-filled array
    mask[currentImage.imagemask] = 1  # set the mask pixels to one where the body is
    plt.imshow(mask, cmap='bone', interpolation='nearest')
    plt.show()

    mask_area_px = np.sum(mask)  # count the number of pixels in the mask, corresponds to the area (in pixels)
    # A = PI * d^2 /4  -> d^2 = 4A/PI -> d = 2*SQRT(A/PI)
    # O = PI * d -> PI * 2 * SQRT(A)/SQRT(PI) -> 2*SQRT(A*PI)
    mask_eqv_diameter_px = 2.0*math.sqrt(mask_area_px/math.pi)
    mask_eqv_diameter_cm = mask_eqv_diameter_px*currentImage.pixelspacing[0]/10.0
    mask_eqv_circumference_px = 2.0*math.sqrt(mask_area_px*math.pi)
    mask_eqv_circumference_cm = mask_eqv_circumference_px*currentImage.pixelspacing[0]/10.0
    print(pixel_size)
    # print('Equal area, effective diameter (px): {:.2f}'.format(mask_eqv_diameter_px))
    print('Equal area, effective diameter (cm): {:.2f}'.format(mask_eqv_diameter_cm))  # .1 egentligen men .2 medan man testar
    # print('Equal area, effective circumference (px):  {:.2f}'.format(mask_eqv_circumference_px))
    print('Equal area, effective circumference (cm):  {:.2f}'.format(mask_eqv_circumference_cm))

    # store calculated values in Image object:
    currentImage.imageposition = currentImage.imageLocation[2]
    currentImage.body_area_px = mask_area_px
    currentImage.equal_area_diameter_px = mask_eqv_diameter_px
    currentImage.equal_area_diameter_cm = mask_eqv_diameter_cm
    currentImage.equal_area_circumference_cm = mask_eqv_circumference_cm

    # calculate average HU of the image slice (/pixel)
    imagedata = currentImage.imageslice
    image = imagedata.copy()
    image[~mask] = 0  # set HU to zero outside the body
    HUsum_px = np.sum(image)
    # HUaverage = HUsum_px/mask_area_px
    HUmean = np.mean(image[mask])
    HUmedian = np.median(image[mask])
    #  print('Average slice HU, per pixel: {:.1f}'.format(HUaverage))
    print('Mean slice HU, per pixel: {:.1f}'.format(HUmean))
    print('Median slice HU, per pixel: {:.1f}'.format(HUmedian))
    # Aw = 1/1000 * AvgCT*Aroi + Aroi  from AAPM Report 220 §2.1
    # Aw = sum(CT(x,y))/1000 * Apixel + sum(Apixel)  from AAPM Report 220 §2.1
    # -> = sum(CT(x,y))/1000 * Apixel + Npixel*Apixel
    # -> = (sum(CT(x,y))/1000 + Npixel) * Apixel   # Apixel = area of pixel = pixelspacing[0]*pixelspacing[1]
    Aw_px = HUsum_px/1000.0 + mask_area_px
    Aw_cm = Aw_px*currentImage.pixelspacing[0]*currentImage.pixelspacing[1]/100.0  # detta används inte
    WED_px = 2.0*math.sqrt(Aw_px/math.pi)
    WED_cm = WED_px*currentImage.pixelspacing[0]/10.0
    # print('Water equivalent area (px): {:.1f}'.format(Aw_px))
    print('Water equivalent area (cm): {:.1f}'.format(Aw_cm))
    # print('Water equivalent diameter, WED (px): {:.1f}'.format(WED_px))
    print('Water equivalent diameter, WED (cm): {:.1f}'.format(WED_cm))

    # store calculated values in Image object:
    currentImage.mean_HU = HUmean
    currentImage.median_HU = HUmedian
    currentImage.WED_px = WED_px
    currentImage.WED_cm = WED_cm

    # AP & LAT from image mask
    # calculate the extent of the patient in image slice
    # how many pixels tall and wide the patient spans
    # calculate the corresponding lengths
    collapse_rows_lat = np.sum(mask,axis=0)
    lat_px = np.count_nonzero(collapse_rows_lat)
    lat_cm = lat_px*currentImage.pixelspacing[0]/10.0
    collapse_columns_ap = np.sum(mask,axis=1)
    ap_px = np.count_nonzero(collapse_columns_ap)
    ap_cm = ap_px*currentImage.pixelspacing[1]/10.0
    # print('LAT (px): {:.2f}'.format(lat_px))
    # print('AP (px):  {:.2f}'.format(ap_px))
    print('LAT (cm): {:.2f}'.format(lat_cm))  # .1 egentligen men .2 medan man testar
    print('AP (cm):  {:.2f}'.format(ap_cm))
    print('LAT + AP (cm): {:.2f}'.format(lat_cm + ap_cm))

    # store calculated values in Image object:
    currentImage.AP_cm = ap_cm
    currentImage.LAT_cm = lat_cm

    # Elliptical efficient diameter as suggested in AAPM Report 204
    # calculated from the AP and LAT measurements
    elliptical_ED_px = math.sqrt(ap_px*lat_px)  # AAPM Report 204
    elliptical_ED_cm = elliptical_ED_px*currentImage.pixelspacing[0]/10.0
    # print('Elliptical, effective diameter (px): {:.2f}'.format(elliptical_ED_px))
    print('Elliptical, effective diameter (cm): {:.2f}'.format(elliptical_ED_cm))  # .1 egentligen men .2 medan man testar

    # store calculated values in Image object:
    currentImage.elliptical_ED_cm = elliptical_ED_cm

    # ... and show plot for how AP and LAT were found
    # plt.plot(collapse_rows_lat, 'r--')    # wider -> = LAT
    # plt.plot(collapse_columns_ap, 'b--')  # less wide  -> = AP
    # plt.show()

    # draw a circle with the area efficient diameter
    # and a horizontal line representing the WED
    # centered at the mask centre of mass.
    # calculate center of mass of the mask
    mask_CM = center_of_mass(mask)
    # print('Center of mass: ' + str(mask_CM))
    CM_r = int(round(mask_CM[0]))
    CM_c = int(round(mask_CM[1]))
    print('Geometrical center: {}, {}'.format(CM_r,CM_c))
    currentImage.geometrical_centre = (round(mask_CM[0],1), round(mask_CM[1],1))
    # circle with corresponding area
    mask_cicle_perimeter = circle_perimeter(CM_r, CM_c, int(round(0.5*mask_eqv_diameter_px)))
    HUmin = image.min()
    HUmax = image.max()
    image[~mask] = HUmin
    image[mask_cicle_perimeter] = HUmax
    # Line corresponding to WED through the CM
    L_r = CM_c-int(round(0.5*WED_px))
    L_l = CM_c+int(round(0.5*WED_px))
    image[CM_r-1:CM_r+1,L_r:L_l] = HUmax
    plt.title('Circle: area equivalent diameter. Line: WED')
    plt.imshow(image, cmap='bone', interpolation='nearest')
    plt.show()

    print('Performing Radon transformation.... please wait')
    mask_radon = radon(mask)
    radon_max_px = 0
    radon_max_degree = 0
    radon_min_px = np.size(mask_radon,axis=0)
    # print(radon_min_px)
    radon_min_degree = 0
    for rot_degree in range(0,180):
        non_zero_px = np.count_nonzero(mask_radon[:, rot_degree])
        if non_zero_px > radon_max_px:
            radon_max_px = non_zero_px
            radon_max_degree = rot_degree
        if non_zero_px < radon_min_px:
            radon_min_px = non_zero_px
            radon_min_degree = rot_degree
    # print('Radon max (px): ' + str(radon_max_px) + ' @ ' + str(radon_max_degree) + ' degrees')
    # print('Radon min (px): ' + str(radon_min_px) + ' @ ' + str(radon_min_degree) + ' degrees')
    radon_max_cm = radon_max_px*currentImage.pixelspacing[0]/10.0
    radon_min_cm = radon_min_px*currentImage.pixelspacing[0]/10.0
    print('Radon max (cm): ' + str(round(radon_max_cm,1)) + ' @ ' + str(radon_max_degree) + ' degrees')
    print('Radon min (cm): ' + str(round(radon_min_cm,1)) + ' @ ' + str(radon_min_degree) + ' degrees')
    # print('Radon max (cm): {:.2f}'.format(radon_max_cm))
    # print('Radon min (cm): {:.2f}'.format(radon_min_cm))
    radon_elliptical_ED_px = math.sqrt(ap_px*lat_px)  # AAPM Report 204
    radon_elliptical_ED_cm = radon_elliptical_ED_px*currentImage.pixelspacing[0]/10.0
    print('Radon, elliptical, effective diameter (cm): {:.2f}'.format(radon_elliptical_ED_cm))

    # store the results from the Radon transform also:
    currentImage.sinogram_LAT_cm = radon_max_cm
    currentImage.sinogram_LAT_degree = radon_max_degree
    currentImage.sinogram_AP_cm = radon_min_cm
    currentImage.sinogram_AP_degree = radon_min_degree
    currentImage.sinogram_ED_cm = radon_elliptical_ED_cm

    # plt.imshow(mask_radon, cmap='bone', interpolation='nearest')
    # plt.show()


def estimateScout(currentImage=None):
    """Calculate the patient size for localizer images

    This function is implemented for localizer images.
    It calculates:
     - the lateral (LAT) or posteriaterio (AP) measure of the patient
     - the localizer-WED as a variant of Menke 2005 10.1148/radiol.2362041327

    Currently, the function also exports high reolution results in the form of two txt-files per localizer image.

    The patient size values are stored in the image object.

    Usage:
        estimateScout(self.currentImage)

    Uses:
        numpy->np.zeros_like
        numpy->np.shape
        numpy->np.nonzero
        numpy->np.count_nonzero
        numpy->np.zeros
        numpy->np.average
        numpy->np.sum
        SSDEexport.py->saveLocalizerData(currentImage)
        SSDEexport.py->Menke2005(currentImage)

    Used by:
        SSDEmain.py


    :param currentImage:
    :return: Sets the corresponding values for patient size in the Image object (currentImage)
             and saves two txt-files with high resolution results for each localizer.
    """

    pixel_size_mm = currentImage.pixelspacing[0]  # x- and y-distance between pixels (in mm according to the standard)
    # print('Pixel spacing:')
    # print(currentImage.pixelspacing)
    mask = np.zeros_like(currentImage.imagemask)  # make a zero-filled array
    mask[currentImage.imagemask] = 1  # set the mask pixels to one where tho body is
    plt.imshow(mask, cmap='bone', interpolation='nearest')
    plt.show()

    rows, columns = np.shape(mask)
    row_i = round(0.5*rows)
    # print('row_i: ' + str(row_i))
    midrow = mask[row_i, :]
    L_i = np.nonzero(midrow)
    # print('First: ' + str(L_i[0][0]) + ' Last: ' + str(L_i[0][-1]))
    L_px = np.count_nonzero(midrow)
    # print('L_px: ' + str(L_px))
    L_cm = L_px*pixel_size_mm/10.0
    print('L_cm: {:.2f}'.format(L_cm))
    # @ position = 'upper left z position' - pixels * pixel_spacing, '-' as pos decreases towards the feet
    # print(currentImage.imageLocation[2])
    L_pos = currentImage.imageLocation[2] - row_i*pixel_size_mm  # I think this i correct :S
    print('L_pos: {:.1f}'.format(L_pos))

    # store results in Image object:
    currentImage.imageposition = L_pos
    if currentImage.sideview:
        currentImage.AP_cm = L_cm
        print(currentImage.AP_cm)
    else:
        currentImage.LAT_cm = L_cm
        print(currentImage.LAT_cm)

    # store calculated values in Image object:
    # currentImage.equal_area_diameter_cm =
    # loop through all the rows and make a list with position and the size, may be store the mid position also
    L_results = np.zeros([rows, 2])
    number_of_rows = range(rows)  # make an iterable object
    for row in number_of_rows:
        L_px = np.count_nonzero(mask[row, :])
        L_pos = currentImage.imageLocation[2] - row * pixel_size_mm
        # for each row, calculate line length and store it with its z-position
        # if the row is empty, skip the calculation of L_cm
        if L_px:
            L_cm = L_px * pixel_size_mm / 10.0
            L_results[row, 0] = L_pos
            L_results[row, 1] = L_cm
        else:
            L_results[row, 0] = L_pos
            L_results[row, 1] = 0.0

    # save the 'high reolution L data'
    currentImage.LocalizerResults = L_results
    saveLocalizerData(currentImage)  # in SSDEexport.py


    bilddata = currentImage.imageslice
    image = bilddata.copy()
    image[~currentImage.imagemask] = 0  # set the image pixels to zero where tho body is not
    image_midrow = image[row_i, :]
    L_avg = np.average(image[row_i,L_i])  # medel av elementen som täcks av linjen, AAPM Report 220 §2.2 (includes table for LAT!)
    # print('L_avg: {:.2f}'.format(L_avg))
    L_sum = np.sum(image_midrow)  # summan av pixelvärdena under linjen
    # print('linjens pixelvärden:')
    # plt.plot(image_midrow)
    plt.show()
    L_length = L_i[0][-1] - L_i[0][0] + 1  # skillnaden mellan pixelpunkterna plus en är antalet pixlar
    L_avg2 = L_sum/L_length
    print('L_avg, average pixel value of line: {:.2f}'.format(L_avg))
    # print('L_sum, pixel value sum of line: {:.2f}'.format(L_sum))
    # print('L_length, lenght of line (px): {:.2f}'.format(L_length))
    # print('L_sum/L_length, average of line: {:.2f}'.format(L_avg2))
    Aroi = L_i[0][-1] - L_i[0][0] + 1  # len(L_i) equals Aroi_px in no pixels have value 0
    # print('Aroi, lenght of line (px): {:.2f}'.format(Aroi))
    L_WED_px = (0.001*L_avg + 1.0)*Aroi  # Menke2005 yields reasonable results
    print('L_WED, Menke (px): {:.2f}'.format(L_WED_px))
    L_WED_cm = L_WED_px*pixel_size_mm/10.0
    print('L_WED, Menke (cm): {:.2f}'.format(L_WED_cm))

    # AAPM Report 220 method, does not seem to yield a reasonable result. Maybe I don´t understand it?
    # L_WED_px = 2.0*math.sqrt((0.001*L_avg + 1.0)*Aroi/math.pi)  # AAPM Report 220 does not yield reasonable results
    # L_WED_px2 = 2.0*math.sqrt((0.001*L_avg2 + 1.0)*Aroi/math.pi)
    # print('L_WED2 (px): {:.2f}'.format(L_WED_px2))
    # L_WED_cm2 = L_WED_px2*pixel_size_mm/10.0
    # print('L_WED2 (cm): {:.2f}'.format(L_WED_cm2))

    # draw the mid pos line and show the image
    image_min = image.min()
    image[~mask] = image_min  #0  # set HU to zero outside the body
    image[row_i-1:row_i+1,L_i[0][0]:L_i[0][-1]] = 1200  # enklast att 'rita linjen' direkt i numpy-matrisen, ge den lite bredd
    plt.imshow(image, cmap='bone', interpolation='nearest')
    # plt.plot(row_i,L_i[0][0], row_i,L_i[0][-1], color='r', linewidth=2.0)  # lines = plt.plot(x1, y1, x2, y2)
    plt.show()

    # store results in Image object:
    currentImage.WED_px = L_WED_px
    currentImage.WED_cm = L_WED_cm

    # Estimate WED from the localizer image
    Menke2005(currentImage)


def Menke2005(currentImage=None):
    """ Estimate WED from a localizer image

    Notice that this function has to use system-specific calibration. i.e pixel values for water & air.

    Estimates the water equivalent diameter (WED) from a localizer image.
    The method is an adaptation of the method described in AAPM report 220 and initially
    by Menke 2005 http://dx.doi.org/10.1148/radiol.2362041327
    The method assumes a linear distribution of pixel values between the 'air pixel value' and the 'water pixel value'.
    The width of the image mask is scaled by a 'WED factor'.
    The 'WED factor' is the scaled pixel value average of the image pixel values for the 'patient width line'.
    The 'WED factor' would be 1.0 of the patient attenuation is 'water equivalent'.


    Usage:
        Menke2005(currentImage)

    Uses:
        numpy.py->np.shape
        numpy.py->np.zeros
        numpy.py->np.nonzero
        numpy.py->np.count_nonzero
        numpy.py->np.average
        SSDEexport.py->saveMenke2005Data()

    Used by:
        Menke2005(currentImage)

    :param currentImage:
    :return:
    """


    print('Starting Menke2005 for every row')

    pixel_size_mm = currentImage.pixelspacing[0]  # x- and y-distance between pixels (in mm according to the standard)

    bilddata = currentImage.imageslice
    image = bilddata.copy()
    image[~currentImage.imagemask] = 0  # set the image pixels to zero where tho body is not

    mask = np.zeros_like(currentImage.imagemask)  # make a zero-filled array
    mask[currentImage.imagemask] = 1  # set the mask pixels to one where tho body is
    rows, columns = np.shape(mask)

    pxv_air = -53.0  # decimal zero to avoid DuckTyping to integer
    pxv_water = 43.0
    # pxv_scale = pxv_air - pxv_water
    # pxv_scale = pxv_water - pxv_air

    # manufacturer specific choice of pxv_air and pxv_water
    if currentImage.manufacturer == 'GE MEDICAL SYSTEMS':  # and not currentImage.sideview:
        pxv_air = -310.0
        pxv_water = 100.0
        print('GE Lightspeed')
    if currentImage.manufacturer == 'Philips':
        pxv_air = -900.0
        pxv_water = 50.0
        print('Philips')
    if currentImage.manufacturer == 'SIEMENS':
        pxv_air = -53.0
        pxv_water = 43.0
        print('SIEMENS')

    # pxv_scale = pxv_air - pxv_water
    pxv_scale = pxv_water - pxv_air

    # calculate Menke2005 WED for all rows
    Menke2005_results = np.zeros([rows, 2])
    number_of_rows = range(rows)  # make an iterable object
    for row_i in number_of_rows:
        row_mask_copy = mask[row_i, :]
        nonZero_i = np.nonzero(row_mask_copy)
        # medel av elementen som täcks av linjen, AAPM Report 220 §2.2 (includes table for LAT!)
        # row_avg = np.average(image[row_i, nonZero_i])
        nonZero_px = np.count_nonzero(mask[row_i, :])
        row_pos = currentImage.imageLocation[2] - row_i*pixel_size_mm
        # for each row, calculate line length and store it with its z-position
        # if the row is empty, skip the calculation of Menke2005_WED_cm
        if nonZero_px:
            # row_pos_cm = nonZero_px*pixel_size_mm/10.0  # change to Menke2005 WED
            # medel av elementen som täcks av linjen, AAPM Report 220 §2.2 (includes table for LAT!)
            row_avg = np.average(image[row_i, nonZero_i])
            Aroi = nonZero_i[0][-1] - nonZero_i[0][0] + 1  # the Area of the ROI is the pixels of the line
            # Menke2005 yields reasonable results (WHEN SCALED!)
            Menke2005_WED_px = (row_avg - pxv_air)/pxv_scale * Aroi
            Menke2005_WED_cm = Menke2005_WED_px*pixel_size_mm/10.0
            Menke2005_results[row_i, 0] = row_pos
            Menke2005_results[row_i, 1] = Menke2005_WED_cm
        else:
            Menke2005_results[row_i, 0] = row_pos
            Menke2005_results[row_i, 1] = 0.0

    print('Finished Menke2005 for every row')
    # save the 'high resolution Menke2005 data'
    currentImage.Menke2005Results = Menke2005_results
    saveMenke2005Data(currentImage)  # in SSDEexport.py


def updateResults(tab=None, currentImage=None):
    """ Service function that updates the Results tab

    Service function that updates the information on the Results tab
    with the calculated results stored in the current image.

    It is first called when the image is chosen and might then show None mostly,
    if the image has not yet been sent to any calculation function previously.
    The function is also called upon after calculations have been performed and
    will then show the results if the are present.

    Usage: updateResults(self, self.currentImage)

    Uses: math->sqrt()
          QtGui->QPixmap.fromImage()
          QtGui->QLabel.setPixmap()

    Used by: SSDEmain.py->MainW.onstoreButton_released()


    :param currentImage: chosen image
    :return: updates the Results tab
    """
    tab.lineEdit_ImageType.setText(currentImage.type)

    # Show the image thumbnail in a label
    QimageThumb = currentImage.thumbnail
    PixmapThumb = QtGui.QPixmap.fromImage(QimageThumb)
    tab.label_Image.setPixmap(PixmapThumb)
    tab.label_Image.show()

    # if currentImage.imageposition:
    #     tab.lineEdit_ImagePosition.setText(str(round(currentImage.imageposition,1)))
    # else:
    #     tab.lineEdit_ImagePosition.setText("")
    if currentImage.equal_area_diameter_cm:
        tab.lineEdit_EAED.setText(str(round(currentImage.equal_area_diameter_cm, 1)))
        tab.lineEdit_CF16_EAED.setText(str(round(ED_to_CF_16cm(currentImage.equal_area_diameter_cm), 2)))
        tab.lineEdit_CF32_EAED.setText(str(round(ED_to_CF_32cm(currentImage.equal_area_diameter_cm), 2)))
    else:
        tab.lineEdit_EAED.setText("")
        tab.lineEdit_CF16_EAED.setText("")
        tab.lineEdit_CF32_EAED.setText("")
    if currentImage.LAT_cm:
        tab.lineEdit_LAT.setText(str(round(currentImage.LAT_cm, 1)))
        tab.lineEdit_CF16_LAT.setText(str(round(LAT_to_CF_16cm(currentImage.LAT_cm), 2)))
        tab.lineEdit_CF32_LAT.setText(str(round(LAT_to_CF_32cm(currentImage.LAT_cm), 2)))
    else:
        tab.lineEdit_LAT.setText("")
        tab.lineEdit_CF16_LAT.setText("")
        tab.lineEdit_CF32_LAT.setText("")
    if currentImage.AP_cm:
        tab.lineEdit_AP.setText(str(round(currentImage.AP_cm, 1)))
        tab.lineEdit_CF16_AP.setText(str(round(AP_to_CF_16cm(currentImage.AP_cm), 2)))
        tab.lineEdit_CF32_AP.setText(str(round(AP_to_CF_32cm(currentImage.AP_cm), 2)))
    else:
        tab.lineEdit_AP.setText("")
        tab.lineEdit_CF16_AP.setText("")
        tab.lineEdit_CF32_AP.setText("")
    try:
        APLAT = currentImage.AP_cm + currentImage.LAT_cm
        tab.lineEdit_APLAT.setText(str(round(APLAT, 1)))
        tab.lineEdit_CF16_APLAT.setText(str(round(LAT_AP_to_CF_16cm(APLAT), 2)))
        tab.lineEdit_CF32_APLAT.setText(str(round(LAT_AP_to_CF_32cm(APLAT), 2)))
    except:
        tab.lineEdit_APLAT.setText("")
        tab.lineEdit_CF16_APLAT.setText("")
        tab.lineEdit_CF32_APLAT.setText("")
    if currentImage.elliptical_ED_cm:
        tab.lineEdit_EED.setText(str(round(currentImage.elliptical_ED_cm, 1)))
        tab.lineEdit_CF16_EED.setText(str(round(ED_to_CF_16cm(currentImage.elliptical_ED_cm), 2)))
        tab.lineEdit_CF32_EED.setText(str(round(ED_to_CF_32cm(currentImage.elliptical_ED_cm), 2)))
    else:
        tab.lineEdit_EED.setText("")
        tab.lineEdit_CF16_EED.setText("")
        tab.lineEdit_CF32_EED.setText("")
    if currentImage.imageposition:
        tab.lineEdit_ZPosition.setText(str(round(currentImage.imageposition, 1)))
    else:
        tab.lineEdit_ZPosition.setText("")

    if currentImage.mean_HU:
        tab.lineEdit_MeanHU.setText(str(round(currentImage.mean_HU, 1)))
    else:
        tab.lineEdit_MeanHU.setText("")
    if currentImage.median_HU:
        tab.lineEdit_MedianHU.setText(str(round(currentImage.median_HU, 1)))
    else:
        tab.lineEdit_MedianHU.setText("")
    if currentImage.WED_cm:
        tab.lineEdit_WED.setText(str(round(currentImage.WED_cm, 1)))
        tab.lineEdit_CF16_WED.setText(str(round(ED_to_CF_16cm(currentImage.WED_cm), 2)))
        tab.lineEdit_CF32_WED.setText(str(round(ED_to_CF_32cm(currentImage.WED_cm), 2)))
    else:
        tab.lineEdit_WED.setText("")
        tab.lineEdit_CF16_WED.setText("")
        tab.lineEdit_CF32_WED.setText("")

    if currentImage.sinogram_LAT_cm:
        tab.lineEdit_sinogramLAT.setText(str(round(currentImage.sinogram_LAT_cm, 1)))
        tab.lineEdit_CF16_sinoLAT.setText(str(round(LAT_to_CF_16cm(currentImage.sinogram_LAT_cm), 2)))
        tab.lineEdit_CF32_sinoLAT.setText(str(round(LAT_to_CF_32cm(currentImage.sinogram_LAT_cm), 2)))
        tab.lineEdit_sinogramLATdegree.setText(str(round(currentImage.sinogram_LAT_degree, 1)))
        tab.lineEdit_sinogramAP.setText(str(round(currentImage.sinogram_AP_cm, 1)))
        tab.lineEdit_CF16_sinoAP.setText(str(round(AP_to_CF_16cm(currentImage.sinogram_AP_cm), 2)))
        tab.lineEdit_CF32_sinoAP.setText(str(round(AP_to_CF_32cm(currentImage.sinogram_AP_cm), 2)))
        tab.lineEdit_sinogramAPdegree.setText(str(round(currentImage.sinogram_AP_degree, 1)))
    else:
        tab.lineEdit_sinogramLAT.setText("")
        tab.lineEdit_CF16_sinoLAT.setText("")
        tab.lineEdit_CF32_sinoLAT.setText("")
        tab.lineEdit_sinogramLATdegree.setText("")
        tab.lineEdit_sinogramAP.setText("")
        tab.lineEdit_CF16_sinoAP.setText("")
        tab.lineEdit_CF32_sinoAP.setText("")
        tab.lineEdit_sinogramAPdegree.setText("")

    try:
        sinogramEED = math.sqrt(currentImage.sinogram_AP_cm*currentImage.sinogram_LAT_cm)
        tab.lineEdit_sinogramEED.setText(str(round(sinogramEED, 1)))
        tab.lineEdit_CF16_sinoEED.setText(str(round(ED_to_CF_16cm(sinogramEED), 2)))
        tab.lineEdit_CF32_sinoEED.setText(str(round(ED_to_CF_32cm(sinogramEED), 2)))
    except:
        tab.lineEdit_sinogramEED.setText("")
        tab.lineEdit_CF16_sinoEED.setText("")
        tab.lineEdit_CF32_sinoEED.setText("")


def clearResults(tab=None):
    """ Service function that clears the fields of the GUI results tab

    Clears the fields of the GUI results tab.
    This function is called when an image is chosen so that the results shown corresponds to the chosen image.

    Usage: clearResults(self)

    Used by: SSDEmain

    Uses:

    :param tab:
    :return:
    """
    tab.label_Image.hide()
    tab.lineEdit_ImageType.setText("")

    tab.lineEdit_ZPosition.setText("")

    tab.lineEdit_EAED.setText("")
    tab.lineEdit_CF16_EAED.setText("")
    tab.lineEdit_CF32_EAED.setText("")

    tab.lineEdit_LAT.setText("")
    tab.lineEdit_CF16_LAT.setText("")
    tab.lineEdit_CF32_LAT.setText("")

    tab.lineEdit_AP.setText("")
    tab.lineEdit_CF16_AP.setText("")
    tab.lineEdit_CF32_AP.setText("")

    tab.lineEdit_APLAT.setText("")
    tab.lineEdit_CF16_APLAT.setText("")
    tab.lineEdit_CF32_APLAT.setText("")

    tab.lineEdit_EED.setText("")
    tab.lineEdit_CF16_EED.setText("")
    tab.lineEdit_CF32_EED.setText("")

    tab.lineEdit_MeanHU.setText("")
    tab.lineEdit_MedianHU.setText("")
    tab.lineEdit_WED.setText("")
    tab.lineEdit_CF16_WED.setText("")
    tab.lineEdit_CF32_WED.setText("")


    tab.lineEdit_sinogramLAT.setText("")
    tab.lineEdit_CF16_sinoLAT.setText("")
    tab.lineEdit_CF32_sinoLAT.setText("")

    tab.lineEdit_sinogramLATdegree.setText("")
    tab.lineEdit_sinogramAP.setText("")
    tab.lineEdit_CF16_sinoAP.setText("")
    tab.lineEdit_CF32_sinoAP.setText("")
    tab.lineEdit_sinogramAPdegree.setText("")

    tab.lineEdit_sinogramEED.setText("")
    tab.lineEdit_CF16_sinoEED.setText("")
    tab.lineEdit_CF32_sinoEED.setText("")