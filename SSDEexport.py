"""
    GPL-3.0

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016 Morgan Nyberg

    This file is part of OpenSSDE.

    OpenSSDE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenSSDE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenSSDE.  If not, see <http://www.gnu.org/licenses/>.
"""
import csv
import datetime
import os.path
from conversionFunctions import *
import numpy as np

def exportRow(currentImage=None):
    """ export a row of results from the current image to a daily file

    Appends a row of results from the current image to a daily csv file.
    The filename consists of a base + date + .csv
    "C:\SSDEresults\SSDE_results" + datestr + ".csv"

    Usage:
        SSDEexport.exportRow(self.currentImage)

    Uses:
        datetime.py->date.today
        datetime.py->.isoformat()
        csv.py->.writer()
        csv.py->.writerow()

    Used by:
        SSDEmain.py->MainW.on_storeButton_released()

    :param currentImage:
    :return: appends results from the current image to a daily file
    """

    # set up the filename:
    datedata = datetime.date.today()
    datestr = datedata.isoformat()
    output_result_filename_raw = "C:/SSDE_results/SSDE_results_" + datestr + ".csv"
    output_result_filename = os.path.normcase(output_result_filename_raw)

    if currentImage.type == "AXIAL":
        # set up the header list:
        headerstr = ["Study Instance UID"]
        headerstr += ["mask success"]
        headerstr += ["Image type"]
        headerstr += ["Manufacturer"]
        headerstr += ["Model"]
        headerstr += ["kVp"]
        headerstr += ["mAs"]
        headerstr += ["Slice Thickness"]
        headerstr += ["CTDIvol"]
        headerstr += ["Table Height"]
        headerstr += ["Z position"]
        headerstr += ["mean HU"]
        headerstr += ["median HU"]
        headerstr += ["WED (cm)"]  # water equivalent diameter
        headerstr += ["CF 16 WED"]
        headerstr += ["CF 32 WED"]
        headerstr += ["LAT (cm)"]
        headerstr += ["CF 16 LAT"]
        headerstr += ["CF 32 LAT"]
        headerstr += ["AP (cm)"]
        headerstr += ["CF 16 AP"]
        headerstr += ["CF 32 AP"]
        headerstr += ["AP + LAT (cm)"]
        headerstr += ["CF 16 AP + LAT"]
        headerstr += ["CF 32 AP + LAT"]
        headerstr += ["EED (cm)"]  # elliptical efficient diameter SQRT(AP*LAT)
        headerstr += ["CF 16 EED"]
        headerstr += ["CF 32 EED"]
        headerstr += ["EAD (cm)"]  # equal area diameter
        headerstr += ["sinogram ED (cm)"]  # SQRT(AP*LAT)
        headerstr += ["CF 16 sino ED"]
        headerstr += ["CF 32 sino ED"]
        headerstr += ["sinogram LAT (cm)"]
        headerstr += ["sinogram LAT (degree)"]
        headerstr += ["CF 16 sino LAT"]
        headerstr += ["CF 32 sino LAT"]
        headerstr += ["sinogram AP (cm)"]
        headerstr += ["sinogram AP (degree)"]
        headerstr += ["CF 16 sino AP"]
        headerstr += ["CF 32 sino AP"]

        # set up the result list:
        resultstr = [currentImage.StudyInstanceUID]
        resultstr += [currentImage.mask_success]
        resultstr += [currentImage.type]
        resultstr += [currentImage.manufacturer]
        resultstr += [currentImage.model]
        resultstr += [currentImage.kvp]
        resultstr += [currentImage.mas]
        resultstr += [currentImage.sliceThickness]
        resultstr += [currentImage.ctdi]
        resultstr += [currentImage.tableHeight]
        resultstr += [currentImage.imageposition]
        resultstr += [round(currentImage.mean_HU, 1)]
        resultstr += [round(currentImage.median_HU, 1)]
        resultstr += [round(currentImage.WED_cm, 1)]
        resultstr += [round(ED_to_CF_16cm(currentImage.WED_cm), 2)]
        resultstr += [round(ED_to_CF_32cm(currentImage.WED_cm), 2)]
        resultstr += [round(currentImage.LAT_cm, 1)]
        resultstr += [round(LAT_to_CF_16cm(currentImage.LAT_cm), 2)]
        resultstr += [round(LAT_to_CF_32cm(currentImage.LAT_cm), 2)]
        resultstr += [round(currentImage.AP_cm, 1)]
        resultstr += [round(AP_to_CF_16cm(currentImage.AP_cm), 2)]
        resultstr += [round(AP_to_CF_32cm(currentImage.AP_cm), 2)]
        APLAT = currentImage.AP_cm + currentImage.LAT_cm
        resultstr += [round(APLAT, 1)]
        resultstr += [round(LAT_AP_to_CF_16cm(APLAT), 2)]
        resultstr += [round(LAT_AP_to_CF_32cm(APLAT), 2)]
        resultstr += [round(currentImage.elliptical_ED_cm, 1)]
        resultstr += [round(ED_to_CF_16cm(currentImage.elliptical_ED_cm), 2)]
        resultstr += [round(ED_to_CF_32cm(currentImage.elliptical_ED_cm), 2)]
        resultstr += [round(currentImage.equal_area_diameter_cm, 1)]
        resultstr += [round(currentImage.sinogram_ED_cm, 1)]
        resultstr += [round(ED_to_CF_16cm(currentImage.sinogram_ED_cm), 2)]
        resultstr += [round(ED_to_CF_32cm(currentImage.sinogram_ED_cm), 2)]
        resultstr += [round(currentImage.sinogram_LAT_cm, 1)]
        resultstr += [round(LAT_to_CF_16cm(currentImage.sinogram_LAT_cm), 2)]
        resultstr += [round(LAT_to_CF_32cm(currentImage.sinogram_LAT_cm), 2)]
        resultstr += [round(currentImage.sinogram_LAT_degree, 1)]
        resultstr += [round(currentImage.sinogram_AP_cm, 1)]
        resultstr += [round(AP_to_CF_16cm(currentImage.sinogram_AP_cm), 2)]
        resultstr += [round(AP_to_CF_32cm(currentImage.sinogram_AP_cm), 2)]
        resultstr += [round(currentImage.sinogram_AP_degree, 1)]
    else:
        # set up the header list:
        headerstr = ["Study Instance UID"]
        headerstr += ["mask success"]
        headerstr += ["Image type"]
        headerstr += ["Manufacturer"]
        headerstr += ["Model"]
        headerstr += ["kVp"]
        headerstr += ["mAs"]
        headerstr += ["Slice Thickness"]
        headerstr += ["CTDIvol"]
        headerstr += ["Table Height"]
        headerstr += ["Z position"]
        headerstr += ["mean HU"]
        headerstr += ["median HU"]
        headerstr += ["WED (cm)"]  # water equivalent diameter
        headerstr += ["CF 16 WED"]
        headerstr += ["CF 32 WED"]
        headerstr += ["LAT (cm)"]
        headerstr += ["CF 16 LAT"]
        headerstr += ["CF 32 LAT"]
        headerstr += ["AP (cm)"]
        headerstr += ["CF 16 AP"]
        headerstr += ["CF 32 AP"]
        headerstr += ["AP + LAT (cm)"]
        headerstr += ["CF 16 AP + LAT"]
        headerstr += ["CF 32 AP + LAT"]
        headerstr += ["EED (cm)"]  # elliptical efficient diameter SQRT(AP*LAT)
        headerstr += ["CF 16 EED"]
        headerstr += ["CF 32 EED"]
        headerstr += ["EAD (cm)"]  # equal area diameter
        headerstr += ["sinogram ED (cm)"]  # SQRT(AP*LAT)
        headerstr += ["CF 16 sino ED"]
        headerstr += ["CF 32 sino ED"]
        headerstr += ["sinogram LAT (cm)"]
        headerstr += ["sinogram LAT (degree)"]
        headerstr += ["CF 16 sino LAT"]
        headerstr += ["CF 32 sino LAT"]
        headerstr += ["sinogram AP (cm)"]
        headerstr += ["sinogram AP (degree)"]
        headerstr += ["CF 16 sino AP"]
        headerstr += ["CF 32 sino AP"]

        # set up the result list:
        resultstr = [currentImage.StudyInstanceUID]
        resultstr += [currentImage.mask_success]
        resultstr += [currentImage.type]
        resultstr += [currentImage.manufacturer]
        resultstr += [currentImage.model]
        resultstr += [currentImage.kvp]
        resultstr += [currentImage.mas]
        resultstr += [' ']  # no slice thickness in localizers, right?
        resultstr += [currentImage.ctdi]
        resultstr += [currentImage.tableHeight]
        resultstr += [currentImage.imageposition]
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [round(currentImage.WED_cm, 1)]
        resultstr += [round(ED_to_CF_16cm(currentImage.WED_cm), 2)]
        resultstr += [round(ED_to_CF_32cm(currentImage.WED_cm), 2)]
        if currentImage.LAT_cm:
            LAT = round(currentImage.LAT_cm, 1)
            LAT16 = round(LAT_to_CF_16cm(currentImage.LAT_cm), 2)
            LAT32 = round(LAT_to_CF_32cm(currentImage.LAT_cm), 2)
        else:
            LAT = ' '
            LAT16 = ' '
            LAT32 = ' '
        resultstr += [LAT]
        resultstr += [LAT16]
        resultstr += [LAT32]
        if currentImage.AP_cm:
            AP = round(currentImage.AP_cm, 1)
            AP16 = round(AP_to_CF_16cm(currentImage.AP_cm), 2)
            AP32 = round(AP_to_CF_32cm(currentImage.AP_cm), 2)
        else:
            AP = ' '
            AP16 = ' '
            AP32 = ' '
        resultstr += [AP]
        resultstr += [AP16]
        resultstr += [AP32]
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']
        resultstr += [' ']

    # If this is the first time we use the file,
    # then lets write a row of headers
    if not os.path.isfile(output_result_filename):
        with open(output_result_filename, mode="w") as csvfile:
            wr = csv.writer(csvfile, dialect='excel-tab', lineterminator='\n')
            wr.writerow(headerstr)
    # append to the file, it would be created if it does not exist
    with open(output_result_filename, mode="a") as csvfile:
        # csvfile.write("testar")
        wr = csv.writer(csvfile, dialect='excel-tab', lineterminator='\n')
        wr.writerow(resultstr)

# exportRow()

def saveLocalizerData(currentImage=None):
    """ Exports high resolution results as a text file.

    Exports high resolution results stored as an np.array in the Image object as a text file.
    The filename contains date of export and a series specific naming convention.

    The text file is set to be saved in the folder 'C:\ssde_results\'

    Usage:
        saveLocalizerData(currentImage)

    Uses:
        datetime.py->date.today
        datetime.py->.isoformat()
        numpy.py->np.shape

    Used by:
        SSDEcalculations.py->estimateScout()

    :param currentImage:
    :return:
    """

    # set up the filename:
    # ex: 'C:/SSDE_results/SSDE_Localizer_results_2016-05-18__PAT-0007 Recon 3 Thorax_frontview.txt'
    base_str = "/SSDE_results/SSDE_Localizer_results_"
    datedata = datetime.date.today()
    date_str = datedata.isoformat()
    PatID_str = currentImage.PatientID
    SerDes_Str = currentImage.SeriesDescription
    if currentImage.sideview:
        view_str = 'sideview'
    else:
        view_str = 'frontview'

    output_result_filename_raw_base = base_str + date_str + '_' + PatID_str + '_' + SerDes_Str + '_' + view_str
    # remove char '.'  and ':' not suitable for filenames
    output_result_filename_raw_base = output_result_filename_raw_base.replace('.', '')
    output_result_filename_raw_base = output_result_filename_raw_base.replace(':', '')
    output_result_filename_raw = 'c:' + output_result_filename_raw_base + ".txt"
    output_result_filename = os.path.normcase(output_result_filename_raw)

    L_results = currentImage.LocalizerResults
    #  file = open("newfile.txt", "w")
    file = open(output_result_filename, "w")
    nr_i, nr_c = np.shape(L_results)
    range_i = range(nr_i)
    for i in range_i:
        file.write(str(L_results[i, 0]) + ' ' + str(L_results[i, 1]) + '\n')
    file.close()


def saveMenke2005Data(currentImage=None):
    """ Exports high resolution results as a text file.

    Exports high resolution results stored as an np.array in the Image object as a text file.
    The filename contains date of export and a series specific naming convention.

    The text file is set to be saved in the folder 'C:\ssde_results\'

    Usage:
        saveMenke2005Data(currentImage)

    Uses:
        datetime.py->date.today
        datetime.py->.isoformat()
        numpy.py->np.shape

    Used by:
        SSDEcalculations.py->Menke2005()


    :param currentImage:
    :return:
    """

    # set up the filename:
    # ex: 'C:/SSDE_results/SSDE_Localizer_results_2016-05-18__PAT-0007 Recon 3 Thorax_frontview.txt'
    base_str = "/SSDE_results/SSDE_Menke2005_results_"
    datedata = datetime.date.today()
    date_str = datedata.isoformat()
    PatID_str = currentImage.PatientID
    SerDes_Str = currentImage.SeriesDescription
    if currentImage.sideview:
        view_str = 'sideview'
    else:
        view_str = 'frontview'

    output_result_filename_raw_base = base_str + date_str + '_' + PatID_str + '_' + SerDes_Str + '_' + view_str
    # remove char '.'  and ':' not suitable for filenames
    output_result_filename_raw_base = output_result_filename_raw_base.replace('.', '')
    output_result_filename_raw_base = output_result_filename_raw_base.replace(':', '')
    output_result_filename_raw = 'c:' + output_result_filename_raw_base + ".txt"
    output_result_filename = os.path.normcase(output_result_filename_raw)

    Menke2005_results = currentImage.Menke2005Results
    #  file = open("newfile.txt", "w")
    file = open(output_result_filename, "w")
    nr_i, nr_c = np.shape(Menke2005_results)
    range_i = range(nr_i)
    for i in range_i:
        file.write(str(Menke2005_results[i, 0]) + ' ' + str(Menke2005_results[i, 1]) + '\n')
    file.close()