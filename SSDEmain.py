"""
    GPL-3.0

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016 Morgan Nyberg

    This file is part of OpenSSDE.

    OpenSSDE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenSSDE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenSSDE.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
# from PyQt4.QtCore import *
from PyQt4 import QtCore
# from PyQt4.QtGui import *
from PyQt4 import QtGui
from qtutils import npArrayToQImage  # för att omvandla en numpy-array till QImage
# from testdcmImportCT import *
from dcmImportCT import *
# from testmodel import Model
from SSDEmodel import Model
from SSDEcalculations import *
import SSDEexport
# import ui_testGraphicsView6  # User interface skapat av QT Designer + 'python mkpyqt.py'
import ui_ssdeMainWindow   # User interface skapat av QT Designer + 'python mkpyqt.py'
import matplotlib.pyplot as plt  # för att visa bildmatris i separat fönster
import numpy as np  # för att visa bildmatris i separat fönster
# from testdcmImportCT import ImageVolume
# import os
# import dicom

# class MainW(QtGui.QMainWindow, ui_testGraphicsView6.Ui_MainWindow):
class MainW(QtGui.QMainWindow, ui_ssdeMainWindow.Ui_MainWindow):
    """ Main loop.


        Usage:

        Uses: self.setupUi
            dcmImportCT.py->DataImporter
            dcmImportCT.py->DataManager
            dcmImportCT.py->Model
            SSDEmain.py->self.treeView.setModel
            SSDEmain.py->self.progressBar.setValue
            SSDEmain.py->self.dropped.connect
            QtCore.pyqtSignal
            dcmImportCT.py->
            dcmImportCT.py->

        Used by:

    """
    dropped = QtCore.pyqtSignal(list)

    def __init__(self, parent=None):
        super(MainW, self).__init__()
        # super(MainW, self).__init__(parent)
        #

        self.currentImage = None
        # self.dirty = False
        self.filename = None
        self.filelist = None

        # setupUI() is provided by the generated module (QT Designer)
        # It creates all specified widgets and layout, and make the connections.
        # It also calls QtCore.QMetaObject.connectSlotsByName(), on_widgetName_signalName
        #
        # BookOfQt4: It is important that you always call the setupUi() method of an instance of
        # a Designer-generated class first, before trying to access member variables of the interface object
        # (... those of the ui).
        self.setupUi(self)

        self.dataImporter = DataImporter()  # From Erland
        # self.analyzer = Analyzer()  # klass från analyzer.py har slots för signaler typ analyzeAll mm
        self.dataManager = DataManager()
        self.model = Model(self.dataManager)  # Model är en klass i testmodel.py
        self.treeView.setModel(self.model)  # has to set up the UI before addressing any of the GUI-objects
        # self.dataImporter.imageFound.connect(self.dataManager.addImage)
        self.treeView.show()
        #  set progressBar to zero
        self.progressBar.setValue(0)

        # old style connect, should use new style:
        # http://pyqt.sourceforge.net/Docs/PyQt4/new_style_signals_slots.html#connect
        # self.connect(self.treeView, SIGNAL("dropped"), self.somethingDropped)
        # self.connect(self, SIGNAL("dropped"), self.somethingDropped)
        # testar nytt sätt, detta kan man flytta upp på rad nära init
        self.dropped.connect(self.somethingDropped)  # funkar tillsammans med dropped = ..signal vid class

    def somethingDropped(self, droppedlink):
        """ used as slot for dropped signal, takes the dropped url and store in (global) list of files

        Usage:

        Uses: os.path.isdir
            SSDEmain.py->self.progressBar.setMaximum
            dcmImportCT.py->validImage(...)
            dcmImportCT.py->DataManager.addImage by self.dataManager.addImage
            self.progressBar.setValue
            self.treeView.sortByColumn

        Used by:
            SSDEmain.py->MainW.dropEvent
            SSDEmain.py->MainW.on_chooseFolderButton_released

        :param droppedlink: fil/filer som droppas från fönsterhanterare
        :return: Lägger till en 'gren' i treeview
        """
        try:
            if os.path.isdir(droppedlink[0]):
                fl = list(findAllFilesQt(droppedlink))
                # NOTE: returns a generator object if list() is not used, but would still work
            else:  # should be: elif file, else break with error code
                fl = droppedlink
                # print('en enskild fil droppades')

            self.filelist = fl
            # spara den droppade filen/filerna i form av en lista
            files = list(self.filelist)
            # self.progressInit.emit(0, len(files))  # ingen progressbar aktiverad än så länge
            #
            # använd Qt Designers progress bar, 'progressBar'
            self.progressBar.setMaximum(len(files)-1)  # -1 räknar upp till 100%  istf 99%
            progressValue = 0
            for ind, path_1 in enumerate(files):
                # self.progressBar.setValue(progressValue)
                success, info = validImage(path_1)
                # print(success)
                if success:
                    # lägg till den lyckligt inlästa filen
                    self.dataManager.addImage(info)
                    # räkna upp progressbar ett steg
                    self.progressBar.setValue(progressValue)
                    # self.dataImporter.imageFound.emit(info)
                progressValue += 1
            # set progressBar to zero again when finished
            self.progressBar.setValue(0)
            # Sortera bilderna i positionsordning (första kolumnen, allt under blir dock i ordning också)
            self.treeView.sortByColumn(0, 0)  # (column, order) order = 0 a-z, 1 z-a
        except:
            print('Drag och släpp filer/mappar från utforskaren till programmet.')

    # @pyqtSignature("")
    def on_chooseFolderButton_released(self):
        """ gets the filename and reads all files in that folder, stores them in the (global) list of files"""
        self.filename = QtGui.QFileDialog.getOpenFileName()
        self.testLineEdit.setText(self.filename)
        # print('klar med filen')
        # Also read the entire folder:
        pathname = os.path.dirname(self.filename)
        print(pathname)
        # make it a list of paths
        pathlist = [pathname]
        fl = list(findAllFilesQt(pathlist))
        self.filelist = fl
        print('klar med filerna i mappen')

        self.dropped.emit(self.filelist)

    def on_chooseImageButton_released(self):
        print('Choose image-knapp')  # for testing

        # välj en bild och visa den
        index = self.treeView.selectedIndexes()  # [0]
        # lagra den valda bilden i en global variabel
        self.currentImage = self.model.getItem(index[0])
        try:
            # om try:et inte 'går igenom' så visas ett tips i textrutan nedan
            self.testLineEdit.setText(self.currentImage.path)
            # visa bilden inuti programmet
            showSlice(self.graphicsView, self.currentImage)  # showSlice in SSDEcalculations, for now
            clearResults(self)
        except:  # borde vara snävare definierat än att fånga upp alla fel, men det funkar så länge
            self.testLineEdit.setText('Choose an image from Images')

    def on_segmentButton_released(self):
        print('Segment button')  # for testing

        print(self.currentImage.type)
        setBusyLamp(self, True)
        if self.currentImage.type == 'AXIAL':
            segmenteraAxial(self.currentImage)  # clean up the code
        elif self.currentImage.type == 'LOCALIZER':
            segmenteraScout(self.currentImage)
        else:
            # plottaBild(self.currentImage)
            self.testLineEdit.setText('No axial or localizer chosen')
        # segmenteraAxial(self.currentImage)
        if self.currentImage.mask_success:
            showContour(self.graphicsView, self.currentImage)
        setBusyLamp(self, False)

    def on_calcButton_released(self):
        print('push button')  # for testing. Antingen väljs en fil via
        # showContour(self.graphicsView, self.currentImage)
        print(self.currentImage.type)
        setBusyLamp(self, True)
        if self.currentImage.type == 'AXIAL':
            calculateEquivDiameter(self.currentImage)  # clean up the code
            showWED(self.graphicsView, self.currentImage)
            try:
                isocenter_deviation(self.currentImage)
            except:
                print('Could not calculate isocenter deviation.')
        elif self.currentImage.type == 'LOCALIZER':
            estimateScout(self.currentImage)
        else:
            print('must be AXIAL or LOCALIZER')
            # plottaBild(self.currentImage)
        setBusyLamp(self, False)


        # segmenteraScout(self.currentImage)
        # segmenteraScoutCanny(self.currentImage)
        #   segmenteraTopogram(self.currentImage)  # finds the threshold value by sweeping window, midpoint between peaks
        #segmenteraTopogramSetValue(self.currentImage)  # simply sets the threshold to -40, better results

    def on_storeButton_released(self):
        print('push button 2')  # for testing
        # showContour(self.graphicsView, self.currentImage)
        setBusyLamp(self, True)
        updateResults(self, self.currentImage)
        SSDEexport.exportRow(self.currentImage)
        setBusyLamp(self, False)

    def on_removeButton_released(self):
        print('removeknapp')  # for testing

        # har inte fått till raderingen
        # ska välja en serie-rad för radering, en dialog ska visas innan radering (ur minnet)
        index = self.treeView.selectedIndexes()  # [0]
        chosenItem = self.model.getItem(index[0])
        # self.model.rowsAboutToBeRemoved(index[0],1,1)  # kanske måste jag implemetera dessa själv i klassen model?
        # self.model.removeRow(1, index[1])  #
        # med bara removeRow händer inget, rowsAboutToBeRemoved och rowsRemoved före och efter också kanske
        # self.model.rowsRemoved(index[0],1,1)
        print('tag bort ' + str(chosenItem))

        """
        void MyModel::operationToRemoveItemAtRow(int row) {
            beginRemoveRows(QModelIndex(), row, row); // no parent, one row to remove

            someListWhichHoldsDataForModel.removeAt(row);

            endRemoveRows();
        }
        """

    def dragEnterEvent(self, event):
        """ checks if dragged items contains url, then it is dropable

        Verifies that an item dragged over the main window contains a url. This is done to make sure that the item is
        "dropable", i.e., that it is an item that can be searched for files to import.

        If it contains a url, the function returns a signal to approve making the item "dropable". Otherwise the return
        signal tells the caller to ignore this item.

        Usage: Gives a visual signal to the user, showing if the item is dropable or not.

        Uses: dcmImport.py-><ClassName>.<FunctionName>()

        :param event:
        :return:
        """

        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """ receives the dropped url and sends the links via the emitted signal



        :param event:
        :return:
        """

        if event.mimeData().hasUrls:
            # print('event.mimeData().hasUrls was true.')  # sann när det droppas en mapp
            # event.setDropAction(Qt.CopyAction)
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))

            # SIGNAL may be 'old style', perhaps find how to make it 'modern style'?
            # self.treeView.emit(SIGNAL("dropped"), links)
            # self.emit(SIGNAL("dropped"), links
            self.dropped.emit(links)
            # self.treeView.dropped.emit(links)

        else:
            event.ignore()

app = QtGui.QApplication(sys.argv)
MainWindow = MainW()

MainWindow.show()
app.exec_()
