"""
    GPL-3.0

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016 Morgan Nyberg

    This file is part of OpenSSDE.

    OpenSSDE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenSSDE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenSSDE.  If not, see <http://www.gnu.org/licenses/>.
"""
import math


# AAPM Report 204, Appendix A: Pertinent Equations.
# For Figures 4, 5, 6, the exponential eqution is given by:
#   y = a * exp(-b*x)
#
# |_Fig_Nr_|_X__________________|_Y_________________|_a________|_b__________|
# |   4    | Eff. Diameter (cm) | Conversion factor | 3.704369 | 0.03671937 |
# |   5    | Eff. Diameter (cm) | Conversion factor | 4.378094 | 0.04331124 |
# |   6    | Eff. Diameter (cm) | Conversion factor | 1.874799 | 0.03871313 |


def ED_to_CF_32cm(x):  # function4(x):  # maybe better names later
    """ The normalized dose coefficient for the 32 cm PMMA CDTIvol phantom
        as a function of effective diameter. 120 keV. AAPM Report 204 figure 4.
        function4(x): Table 1D, effective diameter -> conversion factor
        function4(function7(x)): Table 1C, AP -> conversion factor
        function4(function8(x)): Table 1B, LAT -> conversion factor
        function4(function9(x)): Table 1A, LAT+AP -> conversion factor

    :param x: Effective diameter (cm)
    :return:  Normalized Dose Coefficient
    """
    return 3.704369 * math.exp(-0.03671937*x)


def function5(x):
    """ The normalized dose coefficient for the 32 cm PMMA CDTIvol phantom
        as a function of effective diameter. 80-140 keV. AAPM Report 204 figure 5.
        Not used for any Table

    :param x: Effective diameter (cm)
    :return:  Normalized Dose Coefficient
    """
    return 4.378094 * math.exp(-0.04331124*x)


def ED_to_CF_16cm(x):  # function6(x):
    """ The normalized dose coefficient for the 16 cm PMMA CDTIvol phantom
        as a function of effective diameter. 120 keV. AAPM Report 204 figure 6.
        function6(x): Table 2D , Effective diameter -> Conversion factor 16 mm phantom.
        function6(function7(x)): Table 2C, AP -> Conversion factor 16 mm phantom.
        function6(function8(x)): Table 2B, LAT -> Conversion factor 16 mm phantom.
        function6(function9(x)): Table 2A, LAT+AP -> Conversion factor 16 mm phantom.


    :param x: Effective diameter (cm)
    :return:  Normalized Dose Coefficient
    """
    return 1.874799 * math.exp(-0.03871313*x)
#
# For Figures 7, 8, 9, a second order polynomial equation is used (Fig 9 linear fit)
# y = a + b*x + c*x^2
#
# |_Fig_Nr_|_X__________________|_Y__________________|__a____________|__b____________|__c____________|
# |   7    | AP diam (cm)       | Eff. Diameter (cm) | -3.744858 E0  |  1.671734 E0  | -1.338955 E-2 |
# |   8    | LAT diam (cm)      | Eff. Diameter (cm) |  5.899298 E0  |  3.270494 E-1 |  9.978896 E-3 |
# |   9    | AP + LAT diam (cm) | Eff. Diameter (cm) | -2.03128  E-1 |  4.958912 E-1 |  0            |


def AP_to_ED(x):  # function7(x):
    """ The effective diameter as a function of AP dimension.
        AAPM Report 204 figure 7.
        Table 1C, AP -> Effective diameter. 16 & 32 mm phantoms.

    :param x: AP Dimension (cm)
    :return:  Effective Diameter (cm)
    """
    return -3.744858 + 1.671734*x - 1.338955*0.01*(x**2)

def LAT_to_ED(x):  #function8(x):
    """ The effective diameter as a function of LAT dimension.
        AAPM Report 204 figure 8.
        Table 1B, LAT -> Effective diameter. 16 & 32 mm phantoms.

    :param x: LAT Dimension (cm)
    :return:  Effective Diameter (cm)
    """
    return 5.899298 + 0.3270494*x + 9.978896*0.001*(x**2)

def LAT_AP_to_ED(x):  # function9(x):
    """ The effective diameter as a function of the sum of the anterior posterior
        and lateral dimensions of the patient.
        AAPM Report 204 figure 9.
        Table 1A, LAT+AP -> Effective diameter. 16 & 32 mm phantoms.

    :param x: AP+LAT Dimension (cm)
    :return:  Effective Diameter (cm)
    """
    return -0.203128 + 0.4958912*x
#
# The data shown in Figure 10 show the effective diameter as a function of patient age
# computed from ICRU 74. The computer fit for this relationship was given by:
#
# y = a + b*x^1.5 + c*x^0.5 + d*exp(-x)
#
# where:
# a = 18.788598
# b =  0.19486455
# c = -1.060056
# d = -7.6244784

def Age_to_ED(x):  # function10(x):
    """ The effective diameter (in cm) as a function of age (years).
        AAPM Report 204 figure 10, based on ICRU 74

    :param x: Age (years)
    :return:  Effective Diameter (cm)
    """
    return 18.788598 + 0.19486455*(x**1.5) - 1.060056*(x**0.5) - 7.6244784*math.exp(-x)


def AP_to_CF_32cm(x):

    return ED_to_CF_32cm(AP_to_ED(x))


def LAT_to_CF_32cm(x):

    return ED_to_CF_32cm(LAT_to_ED(x))


def LAT_AP_to_CF_32cm(x):

    return ED_to_CF_32cm(LAT_AP_to_ED(x))


def AP_to_CF_16cm(x):

    return ED_to_CF_16cm(AP_to_ED(x))


def LAT_to_CF_16cm(x):

    return ED_to_CF_16cm(LAT_to_ED(x))


def LAT_AP_to_CF_16cm(x):

    return ED_to_CF_16cm(LAT_AP_to_ED(x))
