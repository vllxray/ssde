"""
    GPL-3.0

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016 Morgan Nyberg, 2013 erlean

    This file is part of OpenSSDE.

    OpenSSDE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenSSDE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenSSDE.  If not, see <http://www.gnu.org/licenses/>.
"""
import dicom
import dicom.UID
# resolve reference to dicom.datadict?
# from dicom.dataset import Dataset, FileDataset
import numpy as np
# import testimageTools
from SSDEimageTools import arrayToQImage, pixelArray, rebin  # for stuff in validImage()
# import SSDEimageTools
from PyQt4 import QtGui
from PyQt4 import QtCore
import os

thumbnailSize = 16

# From Erlands dataImporter:
# validTags is
validTags = {'patientID': (0x10, 0x20),  # patient ID
             'studyID': (0x20, 0xD),  # study id
             'seriesID': (0x20, 0xE),  # seriesID
             'imageID': (0x8, 0x18),  # imageID
             'imageOrientation': (0x20, 0x37),  # image orientation
             'imageLocation': (0x20, 0x32),  # image location
             'patientPosition': (0x18, 0x5100),  # patient position
             'pixelSpacing': (0x28, 0x30),  # pixel spacing
             'sliceSpacing': (0x18, 0x50),  # slice spacing
             'acquisitionNumber': (0x20, 0x12),  # acu number
             'coordID': (0x20, 0x52),  # FOR ID
             'imageType': (0x008, 0x008),  # ex [’ORIGINAL’, ’PRIMARY’, ’AXIAL’] or [’ORIGINAL’, ’PRIMARY’, ’LOCALIZER’]
             'SOPClassUID': (0x8, 0x16)  # SOPClassUID, i.e. CT-image or RDSR
             }
extraTags = {'seriesDate': (0x8, 0x22),
             'seriesTime': (0x8, 0x31),
             'manufacturer': (0x8, 0x70),
             'modelName': (0x8, 0x1090),
             'kvp': (0x18, 0x60),
             'sFOV': (0x18, 0x90),
             'protocol': (0x18, 0x1030),
             'rFOV': (0x18, 0x1100),
             'exposure': (0x18, 0x1151),
             'exposuretime': (0x18, 0x1150),
             'mas': (0x18, 0x1152),
             'focus': (0x18, 0x1190),
             'kernel': (0x18, 0x1210),
             'ctdiVol': (0x18, 0x9345),
             'seriesNumber': (0x20, 0x11),
             'seriesDescription': (0x8, 0x103e),
             'institutionName': (0x8, 0x80),
             'siteName': (0x8, 0x1010),
             'software': (0x18, 0x1020),
             'bodypart': (0x18, 0x15),
             'filter': (0x18, 0x1160),
             'singlecoll': (0x18, 0x9306),
             'multicoll': (0x18, 0x9307),
             'pitch': (0x18, 0x9311),  # spiral pitch factor
             'rotTime': (0x18, 0x9305),
             # # x, y, and z coordinates (in the patient coordinate system) in mm
             'DataCollectionCenter': (0x18, 0x9313),  # ... of the center of the region in which data were collected
             'ReconstructionTargetCenter': (0x18, 0x9318),  # ... of the reconstruction center target point as used for reconstruction
             'DistanceSourceToDetector': (0x18, 0x1110),  # Distance in mm from source to detector center
             'DistanceSourceToPatient': (0x18, 0x1111),  # Distance in mm from source to center of field of view
             # testa att läsa in privat tagg, också i infoTags_ex, och i image och imageContainer
             # (0x28, 0x0004) men ej (0x1E1, 0x0010), (0xe1, 0x1002)
             'scoutview': (0x19, 0x1026),   # degree (90 or 180) of the xray source during scout
             'RDSRtest': (0x40, 0x275),  # first try to read and extract RDSE-information, more work needed
             'sliceThickness': (0x18, 0x0050),  # slicethickness
             'tableHeight': (0x018, 0x1130),
             'windowCenter': (0x28, 0x1050),  # mittenvärdet för fönstring av bilden, image specific
             'windowWidth': (0x28, 0x1051)    # bredden för fönstring av bilden, image specific
             }
# From Erlands dataManager:

# infoTags will hold the tags used for infoContainer, and consists of validTags+extraTags-infoTags_ex
infoTags = {}
#  infoTags_ex excludes some tags that exists in validTags and extraTags from the Series Info fold out
# for later inclusion in the Images fold out.
infoTags_ex = ['imageID', 'imageLocation', 'ctdiVol', 'studyID',
               'exposure', 'exposuretime', 'imageType',
               'windowCenter', 'windowWidth']
# copy all keys in validTags and extraTags into infoTags unless they are in infoTags_ex, to show in Series Info
for key, val in dict({**validTags, **extraTags}).items():
    if key not in infoTags_ex:
        # Add key (e.g. patientID) and value (e.g. (0x10, 0x20) ) to infoTags
        infoTags[key] = val

# Tags connected to the headers in the GUI tree view
headerTags = [validTags['patientID'],
              extraTags['seriesDescription'],
              extraTags['seriesDate'],
              extraTags['seriesNumber'],
              validTags['acquisitionNumber'],
              validTags['studyID']]


def findAllFilesQt(pathList):  # From: erlean, dataImporter
    """ Receives a URL list and returns file paths.

     Receives a URL list (dropped) and returns the file path.
     If the URL points to a singular file, then a singular file path is returned.
     If the URL points to a directory, the a generator object (similar to a list) containing file paths
     to every file in that directory and all its subdirectory is returned.

     Usage: fl = list(findAllFilesQt(pathlist))

     Uses:
        QtCore.QFileInfo
        QtCore.QDir
        QtCore.QDirIterator

     Used by:
        SSDEmain.py->MainW.on_chooseFolderButton_released

    :param pathList: URL to either a single file path or a directory path.
    :return: Generator object with either a single file path or paths to all files in a directory and its sub dirs.
    """
    for qstpath in pathList:
        # info = QFileInfo(qstpath)
        info = QtCore.QFileInfo(qstpath)
        if info.isFile():
            yield info.canonicalFilePath()
        elif info.isDir():
            # dd = QDir(info.canonicalFilePath())
            # it = QDirIterator(dd, QDirIterator.Subdirectories)
            dd = QtCore.QDir(info.canonicalFilePath())
            it = QtCore.QDirIterator(dd, QtCore.QDirIterator.Subdirectories)
            while it.hasNext():
                it.next()
                fileInfo = it.fileInfo()
                if fileInfo.isFile():
                    yield fileInfo.canonicalFilePath()


def findAllFiles(pathList):  # From: erlean, dataImporter
    pass
    # for path in pathList:
    #     if os.path.isdir(path):
    #         for dirname, dirnames, filenames in os.walk(path):
    #             for filename in filenames:
    #                 # print(filename)
    #                 yield os.path.normpath(os.path.join(dirname, filename))
    #     elif os.path.isfile(path):
    #         yield os.path.normpath(path)



class BaseItem(object):  # Från Erlands dataManager:
    """ Common base class for Root, Series, InfoItem, InfoContainer, ImageContainer, Image, Analysis, and Localization

    The BaseItem handles all the necessary class functions for the hierarchical model
    such as adding, removing, counting and returning children for the model
    and setting, counting and returning data for the treeview.

     Usage: class Series(BaseItem) gives common functionality to model items

     Uses:
        dcmImportCT.py->self.parent.addChild

     Used by:
        dcmImportCT.py->Root
        dcmImportCT.py->Series
        dcmImportCT.py->InfoItem
        dcmImportCT.py->InfoContainer
        dcmImportCT.py->Localization
        dcmImportCT.py->ImageContainer
        dcmImportCT.py->Image
        dcmImportCT.py->Analysis

    """
    def __init__(self, parent=None, ID=None):
        self.parent = parent
        self.ID = ID
        self.children = []
        self.displayData = []
        if parent is not None:
            self.parent.addChild(self)

    # ID and child functions is used for populating the model
    def ID(self):
        return self.ID

    # appends a child to children
    # i.e. 'hangs' a 'sub node', could be of any type derived on BaseItem
    def addChild(self, child):
        self.children.append(child)

    # Returns the index of the node
    # Return 0 if the node is Root (does not have a parent)
    def childNumber(self):
        if self.parent is not None:
            return self.parent.children.index(self)
        return 0

    # removes a node from the model
    def removeChild(self, child):
        index = self.children.index(child)
        del self.children[index]

    # returns the node/child at the requested row
    def child(self, row):
        return self.children[row]

    # returns the current number of nodes/children in the model
    def childCount(self):
        return len(self.children)

    # flags and data is used for construction of the GUI treeview

    # returns the number of labels, for reserving space to show hem in the treeview
    def dataCount(self):
        length = len(self.displayData)
        if length > 0:
            return length
        return 1

    # settings for behavior in the treeview, here the items is set to be selectable and enabled
    def flags(self, column=0):
        qt = QtCore.Qt
        return qt.ItemIsSelectable | qt.ItemIsEnabled

    # stores the values to show in the treeview
    def setData(self, column, value, role):
        if role == QtCore.Qt.DisplayRole:
            while len(self.displayData) <= column:
                    self.displayData.append("")
            self.displayData[column] = value
            return True
        return False

    # populates the treeview, shows the stored values
    def data(self, column, role):
        if role == QtCore.Qt.DisplayRole:
            if column < len(self.displayData):
                return str(self.displayData[column])
                # return QtCore.QVariant(self.displayData[column])
        return str()


class Root(BaseItem):
    """
    Part of the model that holds all the data.
    Root is the invisible point where the TreeView/model is 'hanged'.

     Usage:

     Uses: dcmImportCT.py->BaseItem

     Used by:
    """
    def __init__(self):
        super(Root, self).__init__()


class Series(BaseItem):
    """
    Part of the model that holds all the data.
    Series is child to Root and parent to InfoContainer, ImageContainer, Localization and Analysis.
    The Series node holds data and information common to all the Images in 'en undersökning'

    Usage: Series(parent, ID, imagePath, data, headerTags) creates a new Series for holding data in the model

    Uses:
        dcmImportCT.py->BaseItem
        dcmImportCT.py->InfoContainer
        dcmImportCT.py->Localization
        dcmImportCT.py->Analysis
        dcmImportCT.py->self.displayData.append
        dcmImportCT.py->self.addImage

    Used by: dcmImportCT.py->Series.addImage
    """
    def __init__(self, parent, ID, imagePath, data, headerTags):
        super(Series, self).__init__(parent, ID)

        # print(data)

        self.acquisitionNumber = data[validTags['acquisitionNumber']]

        InfoContainer(self, None, data)  # 'Series Info'
        """ 'data' holds ... during ... """
        AP = data[validTags['patientPosition']][:2] == 'HF'
        self.imageLocation = Localization(self, data[validTags['coordID']], AP)
        """ 'imageLocation'  holds the location of the phantom in the image slice"""
        self.analysis = Analysis(self)
        """ 'analysis' holds the analysis results via the class Analysis in analysisMethods.py, or 'Not Analysed yet'
             this is not what is shown on the row of the image (under Images)
             this is what is shown under 'Series Info' """
        self.imageContainer = ImageContainer(self, None)
        """ 'imageContainer'  holds all the individual image slices and ...
            imageContainer.displayData holds the headers for what is shown for the 'Images' row
            while image.displayData holds corresponding value for the headers (they have to match) """

        self.coordinateID = str(data[validTags['coordID']])
        self.patientOrientation = str(str(data[validTags['patientPosition']]))

        # loop and set headerTags
        for tag in headerTags:
            """ populates the view according to received list of headerTags """
            # print('kollar headerTag: ' + str(tag))  # går igenom fem taggar
            try:
                if tag == extraTags['seriesDate']:
                    # print('Hittar taggen seriesDate och kopierar data[tag]')  # hittar 'seriesDate bland dessa
                    date = data[tag]
                    # print(str(date))  # 'seriesDate' verkar inte finnas som key i data
                    if date == 'Not found':
                        self.displayData.append(str("Not Found"))
                    else:
                        qdate = QtCore.QDate(int(date[:4]), int(date[4:6]),
                                             int(date[6:]))
                        self.displayData.append(qdate.toString("ddd dd.MM.yyyy"))
                elif tag == extraTags['seriesDescription']:
                    if tag not in data:
                        self.displayData.append(str("Series"))
                    elif len(data[tag].strip()) > 0:
                        self.displayData.append(str(data[tag]))
                    else:
                        self.displayData.append(str("Series"))
                else:
                    self.displayData.append(str(data[tag]))
            except KeyError:
                self.displayData.append(str(""))

        # add the slice to the model
        self.addImage(imagePath, data, headerTags)

    def addImage(self, imagePath, data, headerTags):
        """ Adds image to image series.

        Adds the image specified by the image path to the Series which it belongs to
        or creates a new Series if necessary.

        Usage: self.addImage(imagePath, data, headerTags)

        Uses: Series Image

        Used by: Series

        :param imagePath: path to the image
        :param data: contains all the retrieved data from the image file
        :param headerTags: these are needed if a new Series is to be created
        :return:
        """

        # get the IDs of the slices already in the model (and in the imageContainer)
        IDs = [str(child.ID) for child in self.imageContainer.children]
        #  get the ID of the slice to add
        ID = str(data[validTags['imageID']])
        # if already in the model, do nothing and return
        if ID in IDs:
            return

        # discern if not already in the model by comparing the acquisition numbers
        # create a new Series and call this function with the same Image again
        # add the image if a Series with same ID already exists
        # om Series.acquisisionNumber inte är detsamma som det i (image's) data
        if self.acquisitionNumber != data[validTags['acquisitionNumber']]:
            # set parent to same Series as parent (invisible root?)
            parent = self.parent
            # create the new Series
            Series(parent, ID, imagePath, data, headerTags)
        else:
            # if Series already exists
            # create an Image-object with the Series imageContainer as parent
            Image(self.imageContainer, ID, imagePath, data)

    # setAnalysis not used in this program yet (remnant from Erland, might get used later)
    def setAnalysis(self, analysis):
        self.analysis.setAnalysis(analysis)

    def flags(self, column=0):
        return super(Series,
                     self).flags() | QtCore.Qt.ItemIsDragEnabled

    def getImagePaths(self):
        return [child.path for child in self.imageContainer.children]

    def getImagePos(self):
        return [child.pos for child in self.imageContainer.children]

    # not used (yet) in this program, otherwise it'll return stored values necessary to perform calculations
    def getAnalysisData(self):
        dat = {}
        dat['AP'] = self.imageLocation.AP
        dat['paths'] = [p for p in self.getImagePaths()]
        dat['zloc'] = self.getImagePos()
        try:
            dat['offset'] = float(self.imageLocation.displayData[1])
        except ValueError:
            pass
        dat['ID'] = (str(self.ID), self.acquisitionNumber)
        return dat


class InfoItem(BaseItem):
    """ Oneliner

    Long descriptor

    Usage:

    Uses:
        dcmImportCT.py->BaseItem

    Used by:

    """
    def __init__(self, parent, ID):
        super(InfoItem, self).__init__(parent, ID)

    def getTableDataTxt(self):  # använder jag denna funktion någon gång? När/var använder Erland den?
        txt = ' '  # '' # str("")  # kanske detta är vad som visas i TreeView?
        # txt = QtCore.QString("")
        n = len(self.displayData)
        for ind, d in enumerate(self.displayData):
            txt += str(d)
            # txt.append(d)
            if ind < n - 1:
                txt += '\t'
                # txt.append("\t")
        return txt

    def flags(self, column=0):
        return super(InfoItem,
                     self).flags() | QtCore.Qt.ItemIsDragEnabled


class InfoContainer(BaseItem):
    """ Hierarchical parent to InfoItems, and child to Series

    Is parent to all InfoItems which are created according to the list of tags in infoTags

    Usage:

    Uses:
        dcmImportCT.py->infoTags[...]
        dcmImportCT.py->BaseItem
        dcmImportCT.py->InfoItem
        dcmImportCT.py->self.children.sort
        dicom.datadict.dictionary_description

    Used by:
    """
    def __init__(self, parent, ID, info):
        super(InfoContainer, self).__init__(parent, ID)
        self.displayData = ['Series Info', '']
        # print(str(info))

        # nameNr = 1
        # for infovalue in infoTags.values():
        for description, infovalue in infoTags.items():
            child = InfoItem(self, None)
            TagNotFound = True
            try:
                childDisplayDataValue = dicom.datadict.dictionary_description(infovalue)
                TagNotFound = False
            except KeyError:
                # print('Hittade inte ' + str(infovalue))
                pass

            if TagNotFound:
                # childDisplayDataValue = 'Private tag ' + str(nameNr)
                childDisplayDataValue = description
                # nameNr += 1
                # print(childDisplayDataValue)
                # print(str(infovalue))
                # print(str(info[infovalue]))
            child.displayData = [childDisplayDataValue, str(info[infovalue])]
        self.children.sort(key=lambda x: x.displayData[0])


    def getTableDataHtml(self):  # använder jag denna funktion någongång? När/var använder Erland den?
        txt = ' '
        # txt = QtCore.QString("")
        n = len(self.children)
        for ind, child in enumerate(self.children):
            txt += str(child.getTableDataTxt())
            # txt.append(child.getTableDataTxt())
            if ind < n-1:
                txt += '\n'
                # txt.append("\n")
        html = QtCore.Qt.escape(txt)
        html.replace("\t", "<td>")
        html.replace("\n", "\n<tr><td>")
        html.prepend("<table>\n<tr><td>")
        html.append("\n</table>")
        return html

    def flags(self, column=0):
        return super(InfoContainer,
                     self).flags() | QtCore.Qt.ItemIsDragEnabled


# Localization används bara av Erland för fantom-grejer
class Localization(BaseItem):
    def __init__(self, parent, ID, AP):
        super(Localization, self).__init__(parent, ID)
        """
        self.displayData = [QtCore.QString('Phantom Position'),
                           0.,
                           QtCore.QString('Not analysed yet...'),
                           QtCore.QString('')]
        """
        self.displayData = ['Phantom Position',
                            0.,
                            'Not analysed yet...',
                            '']
        self.automaticCorrectionMode = True
        self.automaticCorrection = None
        self.AP = AP        # AnteriorPosterior

    def setAutomaticCorrection(self, value, AP):
        if value != value:
            self.displayData[2] = "Not Found"
            self.automaticCorrection = None
            self.displayData[1] = "Enter manual value"
            self.displayData[3] = str(self.AP)
            """
            self.displayData[2] = QtCore.QString("Not Found")
            self.automaticCorrection = None
            self.displayData[1] = QtCore.QString("Enter manual value")
            self.displayData[3] = QtCore.QString(str(self.AP))
            """
        else:
            self.automaticCorrection = value
            self.displayData[2] = "Automatic"
            self.displayData[1] = str(format(self.automaticCorrection, '.3f'))
            self.displayData[3] = str(AP)
            """
            self.displayData[2] = QtCore.QString("Automatic")
            self.displayData[1] = QtCore.QString(format(
                self.automaticCorrection, '.3f'))
             self.displayData[3] = QtCore.QString(str(AP))
            """
            self.AP = AP

    def setData(self, column, value, role):
        if role == QtCore.Qt.EditRole and column == 1:
            if value == "":
                if self.automaticCorrection is not None:
                    self.displayData[column + 1] = "Automatic"
                    # self.displayData[column + 1] = QtCore.QString("Automatic")
                    if abs(float(self.displayData[column]) -
                           self.automaticCorrection) > .001:
                        self.displayData[column] = self.automaticCorrection
                        return True
                else:
                    self.displayData[column + 1] = "Not Found"
                    self.displayData[column] = "Enter manual value"
                    # self.displayData[column + 1] = QtCore.QString("Not Found")
                    # self.displayData[column] = QtCore.QString("Enter manual "
                    #                                          "value")
                return False
            try:
                value = float(value)
            except ValueError:
                return False
            else:
                try:
                    cval = float(self.displayData[column])
                except ValueError:
                    self.displayData[column] = value
                    self.displayData[column + 1] = "Manual entry"
                    # self.displayData[column + 1] = QtCore.QString("Manual"
                    #                                              " entry")
                    return True
                else:
                    if abs(cval - value) > .001:
                        self.displayData[column] = value
                        self.displayData[column + 1] = "Manual entry"
                        # self.displayData[column + 1] = QtCore.QString("Manual"
                        #                                              " entry")
                        return True
                    else:
                        return False
        return super(Localization, self).setData(column, value, role)

    def data(self, column, role):
        if role == QtCore.Qt.ForegroundRole and column == 1:
            # if self.displayData[1] == QtCore.QString("Enter manual value"):
            if self.displayData[1] == "Enter manual value":
                return QtCore.QVariant(QtGui.QBrush(QtGui.QColor(0,
                                                                 0, 0, 127)))
        return super(Localization, self).data(column, role)

    def flags(self, column=0):
        base_flags = super(Localization, self).flags()
        if column == 1:
            return base_flags | QtCore.Qt.ItemIsEditable
        return base_flags


class ImageContainer(BaseItem):
    """ Node in the model, child to Series, parent to all images

    Part of the model to hold all data, child to Series, parent to all images.
    Is created during init of Series-object.

    ImageContainer.displayData holds the headers (and an ID)
    Image.displayData holds the corresponding values (they have to match)
    The two displayData variables is what is shown in the TreeView.

    Usage:

    Uses:
        dcmImportCT.py->BaseItem

    Used by:
    """
    def __init__(self, parent, ID):
        super(ImageContainer, self).__init__(parent, ID)
        self.displayData = [st for st in ['Images (location)', 'Exposure (mAs)', 'CTDIvol', 'Path', 'Test']]
        # self.displayData = [st for st in ['Images (location)', 'Exposure (mAs)', 'CTDIvol', 'Path', 'Image Type']]
        # self.displayData = [st for st in ['Images', 'Exposure', 'CTDIvol', 'Path', 'Image Type', 'Test']]
        # ImageContainer displayData must/should correspond to image displayData

    def flags(self, column=0):
        return super(ImageContainer,
                     self).flags() | QtCore.Qt.ItemIsDragEnabled


class Image(BaseItem):
    """  Part of the model to hold all data. Image is where you store all image specific data.

    Part of the model to hold all data. Child of ImageContainer. Stores image specific data.
    It is created by a call from Series with the argument 'data' which contains all information read from the file.
    Some of that information is extracted and stored as variables in this object.

    Usage:

    Uses:
        dcmImportCT.py->BaseItem

    Used by:
        dcmImportCT.py->Series.addImage
    """
    def __init__(self, parent, ID, path, data):
        super(Image, self).__init__(parent, ID)

        # local copies for easy access
        self.StudyInstanceUID = data[validTags['studyID']]
        self.PatientID = data[validTags['patientID']]
        self.SeriesDescription = data[extraTags['seriesDescription']]

        # Prepare variables which will be set by later calculations
        self.mask_success = False
        self.patient_clipped = False
        self.imagecontour = None
        self.imagemask = None
        self.LocalizerResults = None  # 'high resolution results'
        self.Menke2005Results = None  # 'high resolution results'

        # kopiera det som ska sparas till imageslice-specifika variablar
        self.path = path
        self.thumbnail = arrayToQImage(data['thumbnail'])
        try:
            self.pos = float(data[validTags['imageLocation']][2])
        except:
            self.pos = 'Not found'
        self.kvp = str(data.get(extraTags['kvp'], 'Not found'))
        self.mas = str(data.get(extraTags['mas'], 'Not found'))  # ska jag casta den till float?
        self.sliceThickness = str(data.get(extraTags['sliceThickness'], 'Not found'))
        self.pitch = str(data.get(extraTags['pitch'], 'Not found'))
        self.ctdi = str(data.get(extraTags['ctdiVol'], 'Not found'))  # ska jag casta den till float?
        self.type = data[validTags['imageType']][2]  # 'AXIAL' or 'LOCALIZER' förhoppningsvis
        self.model = data[extraTags['modelName']]
        self.manufacturer = data[extraTags['manufacturer']]
        self.scoutview = data[extraTags['scoutview']]
        self.tableHeight = data[extraTags['tableHeight']]
        # print('imageLocation (0020,0032) i.e. Image Position (Patient)')
        # print(data[validTags['imageLocation']])
        # print('imageOrientation (Patient) (0020,0037)')
        # print(data[validTags['imageOrientation']])
        # self.imageOrientation = data[validTags['imageOrientation']]
        # print('sliceLocation (0020,1041)')
        # print(data[extraTags['sliceLocation']])
        # print('tablePosition (0x18, 0x9327)')
        # print(data[extraTags['tablePosition']])
        # print('anatomicalOrientation (0x10, 0x2210)')
        # print(data[extraTags['anatomicalOrientation']])

        self.sideview = False
        try:
            self.imageOrientation = data[validTags['imageOrientation']]
            print(self.imageOrientation)
        except:
            self.imageOrientation = 'Not found'
        if not self.imageOrientation == 'Not found':
            xrow, xcol, yrow, ycol, zrow, zcol = self.imageOrientation
            self.sideview = (1.0 - abs(xcol)) < 0.01
            if self.sideview and not self.type == 'AXIAL':
                print('Side view')
            elif not self.type == 'AXIAL':
                print('Not side view')


        # lagra bilden också,
        # imageslice måste läggas in under tagggen 'imageslice' i funktionen validImage (efter success)
        # self.imageslice = testimageTools.arrayToQImage(data['imageslice'])
        self.imageslice = data['imageslice']  # spara bilden som en numpy-array
        self.pixelspacing_str = data[validTags['pixelSpacing']]
        self.imageLocation = data[validTags['imageLocation']]  # x, y, z
        # extra information om position
        self.DataCollectionCenter = data[extraTags['DataCollectionCenter']]
        self.ReconstructionTargetCenter = data[extraTags['ReconstructionTargetCenter']]
        self.DistanceSourceToPatient = data[extraTags['DistanceSourceToPatient']]


        # fetch and store the windowing set by the user and stored in the dicom file
        windowCenterArray = data.get(extraTags['windowCenter'], 'Not found')
        windowWidthArray = data.get(extraTags['windowWidth'], 'Not found')
        # set default values
        if windowCenterArray == 'Not found':
            self.windowCenter = 1.0
            self.windowWidth = 5.0
        else:
            try:
                self.windowCenter = windowCenterArray[0]  # only first window for now
            except IndexError:
                self.windowCenter = windowCenterArray
            try:
                self.windowWidth = windowWidthArray[0]  # only first window for now
            except IndexError:
                self.windowWidth = windowWidthArray

        try:
            ps_x, ps_y = data[validTags['pixelSpacing']]
            self.pixelspacing = ps_x, ps_y
            # print(self.pixelspacing)
        except:
            print('Trouble parsing pixelSpacing')

        # variables concerning patient size
        self.imageposition = None
        self.mean_HU = None
        self.median_HU = None
        self.body_area_px = None
        self.equal_area_diameter_px = None
        self.equal_area_diameter_cm = None
        self.equal_area_circumference_cm = None
        self.WED_px = None
        self.WED_cm = None
        self.AP_cm = None
        self.LAT_cm = None
        self.elliptical_ED_cm = None
        self.geometrical_centre = None
        # distance between patient geometrical centre and 'isocenter' [X_offset_mm, Y_offset_mm]
        self.patient_offset = None
        # and those from the Radon transform
        self.sinogram_AP_cm = None
        self.sinogram_AP_degree = None
        self.sinogram_LAT_cm = None
        self.sinogram_LAT_degree = None
        self.sinogram_ED_cm = None

        # Store in displayData what will be presented in the TreeView for the Image(s)
        self.displayData = [self.pos]  # under Images
        # jag sparar undan mas och ctdi som variablar self.mas resp self.ctdi också
        self.displayData.append(self.mas)  # under Exposure
        self.displayData.append(self.ctdi)  # under CTDIvol
        self.displayData.append(self.url.toLocalFile())  # under Path
        # lagra och visa imageType tillsvidare för verifiering
        imtype = str(data.get(validTags['imageType'], 'Not found'))  # funkar, visas
        self.displayData.append(imtype)
        # self.displayData.append(privCreator)

    @property
    def url(self):
        return QtCore.QUrl.fromLocalFile(self.path)

    # this data function might not be needed anymore, as I solve it in the model module
    def data(self, column, role):
        if role == QtCore.Qt.DecorationRole and column == 0:
            return QtGui.QPixmap().fromImage(self.thumbnail)
        if role == QtCore.Qt.SizeHintRole and column == 0:
            return QtCore.QSize(self.thumbnail.size())
        return super(Image, self).data(column, role)

    def flags(self, column=0):
        return super(Image, self).flags() | QtCore.Qt.ItemIsDragEnabled

# Similar class as Series and Images for storing and presenting results from the Analysis
# not used (yet) by this program
class Analysis(BaseItem):
    """ gissningsvis innehåller denna klass det som visas som gren under Analysis
        parent sätts antagligen som den Series som Analysis tillhör på samma sätt som Image-objektet
        Tror att objektet innehåller två listor, en med bilder/undersökningar och en med vilka analyser
    """
    def __init__(self, parent, ID=None):
        super(Analysis, self).__init__(parent, ID)
        self.analysis = []
        self.valid = False
        self.displayData = [st for st in ['Analysis', 'Not started']]
        # self.displayData = [QtCore.QString(st) for st in ['Analysis',
        #                                                  'Not started']]
        self.uids = []
        print('Analysis initierad')

    def unsetAnalysis(self):
        self.analysis = []
        self.valid = False
        self.uids = []

    def setAnalysis(self, analysis):
        names = [a.name for a in self.analysis]
        if analysis.name in names:
            del self.analysis[names.index(analysis.name)]
            del self.uids[names.index(analysis.name)]
        self.analysis.append(analysis)
        self.analysis.sort(key=lambda x: x.name)
        self.valid = True
        self.uids.append(analysis.imageUids)
        # print('Analysis.setAnalysis visited')

    def data(self, column, role):
        if role == QtCore.Qt.DisplayRole and column == 1:
            if self.valid:
                return 'Analysis Finished'
            else:
                return 'Not Analysed'
        else:
            return super(Analysis, self).data(column, role)


def dicom_type_translator(tag):
    """ Returns the data type of the dicom tag by looking at the tag code.

    Usage:

    Uses: dicom.datadict.dictionaryVM dicom.datadict.dictionaryVR

    Used by:

    :param tag: a dicom tag
    :return: the data type: np.array, str, int, or float
    """
    # läs taggens VM, multiplicity
    try:
        vm = dicom.datadict.dictionaryVM(tag)
        # returnera en np-array om multiplicity inte är '1'
        if vm != '1':
            return np.array
    except:
        # print('kunde inte läsa VM för ' + str(tag))
        pass

    # läs taggens typ och returnera motsvarande, fungerar inte ducktyping?
    # translate and return the tag type
    try:
        tp = dicom.datadict.dictionaryVR(tag)

        if tp in ['LO', 'DA', 'SH', 'UI', 'CS', 'TM']:
            return str
        elif tp in ['IS']:
            return int
        elif tp in ['DS', 'FD']:
            return float
        else:
            return str
    except:
        # print('kunde inte läsa VR för ' + str(tag))
        return str


class SelectionModel(QtGui.QItemSelectionModel):
    """ I do not use this class for selecting. I use the inherited functions of the QAbstractItem class
    Jag använder de nedärvda funktionerna från QAbstractItem istället, så länge (tror jag i alla fall)
    """
    def __init__(self, model, parent=None):
        super(SelectionModel, self).__init__(model, parent)

    @QtCore.pyqtSlot(QtCore.QModelIndex,
                     QtGui.QItemSelectionModel.SelectionFlag)
    @QtCore.pyqtSlot(QtGui.QItemSelection,
                     QtGui.QItemSelectionModel.SelectionFlag)
    def select(self, selection, flag):
        if isinstance(selection, QtCore.QModelIndex):
            super(SelectionModel, self).select(selection, flag)
            return

        indexes = selection.indexes()

        if len(indexes) < 2:
            super(SelectionModel, self).select(selection, flag)
            return

        parent = indexes[0].parent()
        valid_indexes = []
        for index in indexes:
            if index.parent() == parent:
                valid_indexes.append(index)
        selection = QtGui.QItemSelection(valid_indexes[0], valid_indexes[-1])
        super(SelectionModel, self).select(selection, flag)


# class DataImporter(QObject):  # from dataImporter
class DataImporter(QtCore.QObject):  # from dataImporter
    """ I do not use this class for importing
    """
    # progress = QtCore.pyqtSignal(int)
    # progressInit = QtCore.pyqtSignal(int, int)
    imageFound = QtCore.pyqtSignal(tuple)  # kopplar imageFound till en signal
    # imageFound = pyqtSignal(tuple)
    # logMessage = QtCore.pyqtSignal(QtCore.QString)
    # importFinished = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(DataImporter, self).__init__(parent)
        # self.input_queue = Queue()
        # self.output_queue = Queue()

    @QtCore.pyqtSlot(list)
    def importPath(self, urlList):
        strList = [url.toLocalFile() for url in urlList]
        filejob = findAllFilesQt(strList)  # QT For using QT for walking a directory tree

        files = list(filejob)
        # self.progressInit.emit(0, len(files))
        for ind, path_1 in enumerate(files):
            success, info = validImage(path_1)
            # path, data = info
            if success:
                # skicka ut en signal som snappas upp av slot/funktionen dataManager(?).addImage
                self.imageFound.emit(info)
                # log = QtCore.QString("Imported ") + path
            else:
                pass
                # log = QtCore.QString("Not imported ")
                # log += path + QtCore.QString(": ")
                # log += QtCore.QString(data['message'])
            # self.logMessage.emit(log)
            # self.progress.emit(ind + 1)

        # self.progress.emit(ind + 2)
        # self.progressInit.emit(0, 1)
        # self.progress.emit(3)
        # self.importFinished.emit()


class DataManager(QtCore.QObject):  # has to be below/after declaration of objects used for setting up the signals
    """ Manages the data stored in the model.

    Manages the data stored in the model.
    Sets the topmost labels in the view according to the headerTags
    Not all functions in this class are used.
    The most interesting function is addImage.

    Usage:

    Uses: dicom.datadict.dictionary_description

    Used by:
    """
    modelChanged = QtCore.pyqtSignal()
    modelAboutToChange = QtCore.pyqtSignal()
    layoutAboutToBeChanged = QtCore.pyqtSignal()
    layoutChanged = QtCore.pyqtSignal()
    dataChanged = QtCore.pyqtSignal(int, int, object)
    delayedImportFinished = QtCore.pyqtSignal()
    startLocalizer = QtCore.pyqtSignal(Root)
    delayedImportProgressValue = QtCore.pyqtSignal(int)
    delayedImportProgressRange = QtCore.pyqtSignal(int, int)
    exportProgressValue = QtCore.pyqtSignal(int)
    exportProgressRange = QtCore.pyqtSignal(int, int)
    analyze = QtCore.pyqtSignal(Series)
    analyzeAll = QtCore.pyqtSignal(list)
    # message = QtCore.pyqtSignal(QtCore.QString, int)
    # logMessage = QtCore.pyqtSignal(QtCore.QString)
    message = QtCore.pyqtSignal(str, int)
    logMessage = QtCore.pyqtSignal(str)
    seriesHighLight = QtCore.pyqtSignal(Series)
    modelLocked = QtCore.pyqtSignal(bool)
    updateAnalysis = QtCore.pyqtSignal(str, int, list, list)

    def __init__(self, parent=None):
        super(DataManager, self).__init__(parent)
        self.rootItem = Root()
        self.delayedImport = []
        self.modelLock = QtCore.QMutex(mode=QtCore.QMutex.Recursive)  # låser man så här? behöver den låsas?

        """
        self.headerTags = [extraTags['seriesDescription'],
                           extraTags['seriesDate'],
                           extraTags['seriesNumber'],
                           validTags['acquisitionNumber'],
                           validTags['patientID']]
        """
        """
        self.headerTags = [validTags['patientID'],
                           extraTags['seriesDescription'],
                           extraTags['seriesDate'],
                           extraTags['seriesNumber'],
                           validTags['acquisitionNumber']]
        """
        """
        self.headerTags = [validTags['patientID'],
                           extraTags['seriesDescription'],
                           extraTags['seriesDate'],
                           extraTags['seriesNumber'],
                           validTags['acquisitionNumber'],
                           validTags['studyID']]
        """
        self.headerTags = headerTags  # dessa finns nu högst upp i filen, bland de andra tag-listorna
        self.rootItem.displayData = []
        for tag in self.headerTags:
            # translate the tags to standardised dicom names
            h = dicom.datadict.dictionary_description(tag)
            self.rootItem.displayData.append(str(h))  #
            # self.rootItem.displayData.append(QtCore.QString(h))

    def getSeriesData(self):
        """ går igenom alla under-grupper i modellen med start i rootItem
            och kör getAnalysisData för varje element (Series?)

        Usage:

        Uses: child.getAnalysisData

        Used by:

        :return: sätter AnalysisData för varje element i serien/modellen
        """
        return [child.getAnalysisData() for child in self.rootItem.children
                if not child.analysis.valid]

    """
    @QtCore.pyqtSlot()
    def phantomChanged(self):
        self.modelLock.lock()
        for child in self.rootItem.children:
            child.analysis.unsetAnalysis()
        self.analyzeAll.emit(self.getSeriesData())
        self.modelLock.unlock()
    """

    @QtCore.pyqtSlot()
    def importFinished(self):
        print('DataManager.importFinished visited')
        self.modelLock.lock()
        n_images = len(self.delayedImport)
        if n_images > 0:
            self.delayedImportProgressRange.emit(0, n_images-1)
            progress = 0
            while len(self.delayedImport) > 0:
                self.addImage(self.delayedImport.pop(0))
                progress += 1
                self.delayedImportProgressValue.emit(progress)
        self.modelLock.unlock()
        self.startLocalizer.emit(self.rootItem)

    @QtCore.pyqtSlot(bool)
    def localizerFinished(self, running):
        if not running:
            self.modelLock.lock()
            self.analyzeAll.emit(self.getSeriesData())
            self.modelLock.unlock()

    @QtCore.pyqtSlot(bool)
    def lockModel(self, lock):
        pass
#        self.modelLock = lock
#        self.modelLocked.emit(self.modelLock)

    def getImagePaths(self, item):
        if isinstance(item, Series):
            return [child.path for child in item.imageContainer.children]
        elif isinstance(item, ImageContainer):
            return [child.path for child in item.children]
        elif isinstance(item, Image):
            return [item.path]
        return []

    def getInfoTableHtml(self, item):
        try:
            tab = item.getTableDataHtml()
        except AttributeError:
            return ""
        else:
            return tab

    @QtCore.pyqtSlot(int, QtCore.Qt.SortOrder)
    def sort(self, column, order):
        """ sorts the series and the images (the image slices in the series (if invoked) under Images)

        Usage:

        Uses:  self.modelLock.lock self.modelLock.unlock
        self.rootItem.dataCount
        self.rootItem.children.sort
        child.imageContainer.dataCount
        child.imageContainer.children.sort

        Used by:

        :param column: which column to sort by, ex 0
        :param order: which order to sort by, ex alphabetic?
        :return:
        """

        """
        if self.modelLock:
            self.message.emit("Please wait for analysis to complete", 5000)
            return
        """
        self.modelLock.lock()
#        self.lockModel(True)
        order == QtCore.Qt.DescendingOrder
        if self.rootItem.dataCount() > column:
            try:
                self.rootItem.children.sort(key=lambda x: int(x.displayData[column]),
                                            reverse=order)
            except ValueError:
                self.rootItem.children.sort(key=lambda x: x.displayData[column],
                                            reverse=order)

        for child in self.rootItem.children:
            if child.imageContainer.dataCount() > column:
                child.imageContainer.children.sort(key=lambda x:
                                                   x.displayData[column],
                                                   reverse=order)
#        self.lockModel(False)
        self.modelLock.unlock()

    def findSeriesByFOR(self, FOR):
        forList = []
        for series in self.rootItem.children:
            if series.coordinateID == FOR:
                forList.append(series)
        return forList

    def findSeries(self, ID, acN):  # finding a series from ID and acquisition
        for series in self.rootItem.children:
            if series.ID == ID and series.acquisitionNumber == acN:
                return series
        return None

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal:
            if role == QtCore.Qt.DisplayRole:
                return self.rootItem.displayData[section]
        return None

    @QtCore.pyqtSlot(str, int, object)
    def setAnalysis(self, suid, acn, analysis):
        print('DataManager.setAnalysis visited')
        series = self.findSeries(suid, acn)
        series.setAnalysis(analysis)
        row = series.analysis.childNumber()
        column = 0
        self.dataChanged.emit(row, column, series.analysis)
        ana = series.analysis.analysis
        self.updateAnalysis.emit(suid, acn, series.getImagePaths(), ana)

    @QtCore.pyqtSlot(list, float, bool)
    def setAutoCorrection(self, series, value, AP):
        for serie in series:
            imLoc = serie.imageLocation
            imLoc.setAutomaticCorrection(value, AP)
            serie.analysis.unsetAnalysis()
            row = imLoc.childNumber()
            column = 0
            self.dataChanged.emit(row, column, imLoc)

    @QtCore.pyqtSlot(str, int)
    def highLightSeries(self, sID, aID):
        series = self.findSeries(sID, aID)
        if series is None:
            return
        self.seriesHighLight.emit(series)

    @QtCore.pyqtSlot(tuple)
    def addImage(self, info):
        """ Adds an Image to a Series, creates a new Series if necessary.

        Checks if there exists a series for this image.
        If not, then create a new Series object, else calls that Series.addImage to add the image to the series.

        Usage:

        Uses: self.modelLock.lock self.modelAboutToChange.emit self.modelChanged.emit self.modelLock.unlock
        self.findSeries Series (existing)Series.addImage

        Used by:

        :param info: info = (path, data{}) from validImage
        :return: Creates a new Series object under the rootItem and adds an Image object,
                 alt adds an Image to an existing Series
        """

        self.modelLock.lock()
        self.modelAboutToChange.emit()

        path, data = info
        ID = str(data[validTags['seriesID']])
        acNumber = data[validTags['acquisitionNumber']]
        existingSeries = self.findSeries(ID, acNumber)  # returns a rootItem child
        # check if there exists a series for this image,
        # if not, then create a new Series object
        # if there is a Series, then call that Series.addImage to add the image to the series
        if existingSeries is None:
            Series(self.rootItem, ID, path, data, self.headerTags)  # create a new Series with Root as parent
        else:
            existingSeries.addImage(path, data, self.headerTags)  # anropar Series.addImage

        self.modelChanged.emit()
        self.modelLock.unlock()

    def setData(self, item, *args):
        """ Populate and update the TreeView with data from the model.

        Usage:

        Uses: self.modelLock.lock self.findSeriesByFOR s.analysis.unsetAnalysis s.imageLocation.setData
        s.imageLocation.childNumber self.dataChanged.emit self.analyzeAll.emit self.getSeriesData self.analyzeAll.emit
        self.modelLock.unlock

        Used by:

        :param item:
        :param args:
        :return:
        """
        self.modelLock.lock()
        success = item.setData(*args)
        # analyse series after data change
        if success and args[2] == QtCore.Qt.EditRole:
            if isinstance(item, Series):
                aList = self.findSeriesByFOR(item.coordinateID)
                for s in aList:
                    s.analysis.unsetAnalysis()
                    s.imageLocation.setData(*args)
                    row = s.imageLocation.childNumber()
                    self.dataChanged.emit(row, args[1],
                                          s.imageLocation)
                self.analyzeAll.emit(self.getSeriesData())
            else:
                while item is not None:
                    item = item.parent
                    if isinstance(item, Series):
                        aList = self.findSeriesByFOR(item.coordinateID)
                        for s in aList:
                            s.analysis.unsetAnalysis()
                            s.imageLocation.setData(*args)
                            row = s.imageLocation.childNumber()
                            self.dataChanged.emit(row, args[1],
                                                  s.imageLocation)
                        self.analyzeAll.emit(self.getSeriesData())
                        break
        self.modelLock.unlock()
        return success

    # @QtCore.pyqtSlot(QtCore.QString)
    @QtCore.pyqtSlot(str)
    def exportAnalysisImages(self, dest):
        """ Not used (yet) by this program.

        :param dest:
        :return: nothing, but saved files on the drive
        """

        """
        if self.modelLock:
            self.message.emit("Please wait for analysis to complete", 5000)
            return
        """
        self.exportProgressRange.emit(0, 0)
        flist = []
        for series in self.rootItem.children:
            uids = [u for ana in series.analysis.uids for u in ana]
            for im in series.imageContainer.children:
                if im.ID in uids:
                    flist.append(im.url)
        if len(flist) == 0:
            self.message.emit("No images to export", 5000)
            self.exportProgressValue.emit(1)
            return

        dr = QtCore.QDir(path=dest)
        if not dr.exists():
            self.message.emit("ERROR: Make sure export directory exists", 5000)
            self.exportProgressValue.emit(1)
            return

        filobj = QtCore.QFileInfo()
        qfil = QtCore.QFile('')
        n_files = len(flist)
        n = 1
        self.exportProgressRange.emit(0, n_files)
        for f in flist:
            filobj.setFile(f.toLocalFile())
            fname = filobj.fileName()
            i = 1
            while dr.exists(fname):
                # fname = QtCore.QString(filobj.fileName() + '_' + str(i))  # does not use Qstrings in Python 3
                fname = str(filobj.fileName() + '_' + str(i))
                i += 1
            dpath = dr.filePath(fname)
            n += 1
            self.exportProgressValue.emit(n)
            suc = qfil.copy(filobj.filePath(), dpath)
            if suc:
                msg = str('Exported' + filobj.filePath())
                msg += str(' to ' + dpath)
                # msg = QtCore.QString('Exported') + filobj.filePath()
                # msg += QtCore.QString(' to ') + dpath
            else:
                msg = str('Failed copying ' + filobj.filePath())
                msg += str(': ' + str(qfil.error()))
                # msg = QtCore.QString('Failed copying ') + filobj.filePath()
                # msg += QtCore.QString(': ') + QtCore.QString(qfil.error())  # does not use Qstrings in Python 3
            self.logMessage.emit(msg)  # I do not use loggin (yet?)


def validImage(path):
    """ Checks whether the file is a dicom file and of the type we want to import.

    Checks whether the file is a dicom file and of the type we want to import by
    first trying to read the file as a dicom file
    and then tries to read validTags frpm that dicom file.

    Usage: success, info = validImage(path)

    Uses: dicom.readfile()
          dicom.filereader.InvalidDicomError
          dcmImportCT.py->dicom_type_translator()
          dcmImportCT.py->validTags
          dcmImportCT.py->extraTags
          numpy.py->np.array
          numpy.py->np.cross
          numpy.py->np.ones
          SSDEimageTools.py->rebin()

    Used by: SSDEmain.py->MainW->somethingDropped()
             dcmImportCT.py->importPath()

    :param path: path to the file
    :return: success  (path, data)
            success = True if dicom Axial or Localizer
            path = (same as input argument)
            data = dictionary containing validTags, extraTags, imageSlice (numpy array), and pixelSpacing (float)
    """
    data = {}
    success = True
    rdsr_file = False
    try:
        dc = dicom.read_file(str(path))
        # dc = dicom.read_file(qStringToStr(path))  # does not use Qstrings in Python 3
        # tag = (0x8, 0x16)
        # f = dicom_type_translator(tag)
        # v = f(dc[tag].value)
        # print(f)
        # print(v)
    except dicom.filereader.InvalidDicomError:
        data['message'] = 'Not DiCOM'
        success = False
    else:
        tag = (0x8, 0x16)
        f = dicom_type_translator(tag)
        v = f(dc[tag].value)
        # print(v)
        if v == 'X-Ray Radiation Dose SR Storage' or v == 'Enhanced SR Storage':
            rdsr_file = True
            print('RDSR')

        if not rdsr_file:
            for desk, tag in validTags.items():
                # wait until passed normal check for validity until storing extended list of tags
                try:
                    f = dicom_type_translator(tag)  # Translate 'validTags' to dicom style/spelling
                    data[tag] = f(dc[tag].value)    # store translated 'validTags'-value for return (0020,0037)
                except KeyError:
                    ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                    ht += ')'
                    data['message'] = 'Invalid DiCOM, Could not find tag ' + ht
                    success = False
                    # data[tag] = 'Not found'
                    print('Key error test ' + ht)
                except ValueError:
                    if tag is validTags['acquisitionNumber']:
                        data[validTags['acquisitionNumber']] = 1
                    elif tag is validTags['sliceSpacing']:
                        data[validTags['sliceSpacing']] = None
                    else:
                        ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                        ht += ')'
                        data['message'] = 'Invalid DiCOM, Error in tag ' + ht
                        success = False
                        print('ValueError test ' + ht)
                except TypeError:
                    if tag is validTags['acquisitionNumber']:
                        data[validTags['acquisitionNumber']] = 1
                    else:
                        ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                        ht += ')'
                        data['message'] = 'Invalid DiCOM, data type error in tag '
                        data['message'] += ht
                        success = False
                        print('TypeError test ' + ht)

            if success:
                # Test for orientation <-Erland, why not check imageType for AXIAL? I´ll check if LOCALIZER at least.
                orient = dc[validTags['imageOrientation']].value
                xvec, yvec = np.array(orient[:3]), np.array(orient[3:])
                zvec = np.cross(xvec, yvec)
                if abs(zvec[2]) < 0.99:
                    data['message'] = 'Image not Axial'
                    success = False
                    print('Image not axial -> no success')
                if data[validTags['imageType']][2] == 'LOCALIZER':  # reset to success if scout/topogram
                    success = True
                    print('Image is a localizer, let it pass the test')

            if success:
                for desk, tag in extraTags.items():  # copy and store the extraTags

                    try:
                        f = dicom_type_translator(tag)  # Translate 'extraTags' to dicom style/spelling
                        data[tag] = f(dc[tag].value)    # store translated 'extraTags'-value for return (0020,0037)
                    except KeyError:
                        data[tag] = 'Not found'
                    except ValueError:
                        data[tag] = 'ValueError'  # temp handle 'could not convert string to float' (Reconstruction Diameter)

                # store pixelSpacing as float
                pixel_spacing = float(data[validTags['pixelSpacing']][0])
                data['pixelSpacing'] = pixel_spacing
                # convert pixelvalues to Hounsfield values and store the image slice as a numpy array
                image = pixelArray(dc)
                # store the image slice in data
                data['imageslice'] = image
                # extracting/calculating thumbnail
                imRed = rebin(image, (thumbnailSize, thumbnailSize))
                data['thumbnail'] = imRed

        if rdsr_file:  # read and store the tags anyway, do I have to add an empty dummy image also?
            success = True
            for desk, tag in validTags.items():  # copy and store the extraTags
                try:
                    f = dicom_type_translator(tag)  # Translate 'extraTags' to dicom style/spelling
                    data[tag] = f(dc[tag].value)    # store translated 'extraTags'-value for return (0020,0037)
                except KeyError:
                    data[tag] = 'Not found'
                    if tag is validTags['acquisitionNumber']:
                        data[validTags['acquisitionNumber']] = 1
                    elif tag is validTags['sliceSpacing']:
                        data[validTags['sliceSpacing']] = None
                    ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                    ht += ')'
                    print('Key error test ' + ht)
                except ValueError:
                    data[tag] = 'ValueError'
                    if tag is validTags['acquisitionNumber']:
                        data[validTags['acquisitionNumber']] = 1
                    elif tag is validTags['sliceSpacing']:
                        data[validTags['sliceSpacing']] = None
                    ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                    ht += ')'
                    print('Value error test ' + ht)
            for desk, tag in extraTags.items():  # copy and store the extraTags
                try:
                    if tag is extraTags['RDSRtest']:
                        print('RDSRtest tag found')
                        print(tag)
                    f = dicom_type_translator(tag)  # Translate 'extraTags' to dicom style/spelling
                    data[tag] = f(dc[tag].value)    # store translated 'extraTags'-value for return (0020,0037)
                except KeyError:
                    ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                    ht += ')'
                    print('Extra Key error test ' + ht)
                    data[tag] = 'Not found'
                except ValueError:
                    ht = '(' + format(tag[0], 'X') + ',' + format(tag[1], 'X')
                    ht += ')'
                    print('Extra Value error test ' + ht)
                    data[tag] = 'ValueError'
            # construct an empty dummy image
            image_shape = (100,100)
            image = 20 * np.ones(image_shape, dtype=np.int)
            image[50, :] = -10
            image[:, 50] = -10
            data['imageslice'] = image
            imRed = rebin(image, (thumbnailSize, thumbnailSize))
            data['thumbnail'] = imRed

    return success, (path, data)